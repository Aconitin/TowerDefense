local debugging = {}

debugging.active = true
debugging.renderPath = false
debugging.renderGrid = 0
debugging.renderSpriteBounds = false
debugging.renderCursorField = true

function debugging:toggle()
	self.active = not self.active
	print("Debug mode set to " .. tostring(self.active) .. "!")
end

function debugging:drawOverlay(engine, solver, roundKeeper)

	local map = solver.tileMap

	if self.active then

		love.window.setTitle("Tower Defense (Debug mode active)")

		--> Print some debug info!
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.print("Debug Menu:"
							.. "\nctrl+alt+enter: Toggle debug mode"
							.. "\nq or escape or alt+f4: quit"
							.. "\nr: Regenerate map"
							.. "\nf: Toggle rendering grid [" .. tostring(self.renderGrid) .. "]"
							.. "\np: Toggle rendering paths [" .. tostring(self.renderPath) .. "]"
							.. "\nz: Toggle rendering sprite bounds [" .. tostring(self.renderSpriteBounds) .. "]"
							, 5, 5)
		love.graphics.print("Info:"
							.. "\nFPS: " .. love.timer.getFPS() .. " | AvgDt: " .. math.round(love.timer.getAverageDelta(), 5)
							.. "\nGametime: " .. math.round(roundKeeper:getGameTime(), 2)
							.. "\nComponents: " .. engine:getTotalComponentCount()
							.. "\nTiles: " .. map.spriteBatch:getCount()
							.. "\nWaves: " .. roundKeeper:getWavesCount()
							, 5, love.graphics.getHeight()-100)

		--> Conveniently redraw map!
		if self.redrawMap then
			self.redrawMap = false
			if solver.solve then
				map = solver:solve()
			end
			local field = solver.tileMap.playfield
			camera:set(
				{
					viewSize = {w = field.width * globals.unitScale.x, h = field.height * globals.unitScale.y},
					cameraLerpTo = {x = (field.x - 1 + 0.5 * field.width) * globals.unitScale.x, y = (field.y - 1 + 0.5 * field.height) * globals.unitScale.y}
				}, true
			)
		end

	else
		love.window.setTitle("Tower Defense")
	end

end

function debugging:drawWithCamera(engine, solver)

	local map = solver.tileMap

	if self.active then

		--> Render grid!
		if self.renderGrid > 0 then

			-- Mark the (0,0) corner in solid red for reference.
			love.graphics.setColor(255, 0, 0, 255)
			love.graphics.circle("fill", 0, 0, 8)

			-- Render the grid borders.
			love.graphics.setLineWidth(1)
			love.graphics.setColor(0, 255, 255, 150)
			for j = 0, map.height do
				local y = j * globals.unitScale.y
				local x1 = 0
				local x2 = x1 + map.width * globals.unitScale.x
				love.graphics.line(x1, y, x2, y)
			end
			for i = 0, map.width do
				local x = i * globals.unitScale.x
				local y1 = 0
				local y2 = y1 + map.height * globals.unitScale.y
				love.graphics.line(x, y1, x, y2)
			end

			-- Render the logic tiles as semitransparent squares (empty = passable).
			if self.renderGrid == 2 then
				love.graphics.setLineWidth(4)
				for j = 1, map.height do
					for i = 1, map.width do
						local v = map:get(i, j)
						love.graphics.setColor(v.color.r, v.color.g, v.color.b, 100)
						love.graphics.rectangle(v.passable and "line" or "fill", (i - 0.875) * globals.unitScale.x, (j - 0.875) * globals.unitScale.y, 0.75 * globals.unitScale.x, 0.75 * globals.unitScale.y)
					end
				end
			end

		end

		--> Render path!
		if self.renderPath then
			-- Draw map entrances and altars.
			love.graphics.setColor(255, 0, 0, 100)
			for _, ee in pairs(map.entrances) do
				local pos = map:tileIndicesToWorldCoords{i = ee.x, j = ee.y}
				love.graphics.circle("fill", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, ee.r * globals.unitScale.x, 25)
				love.graphics.circle("fill", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, 8, 25)
			end
			love.graphics.setColor(0, 0, 255, 100)
			for _, ee in pairs(map.altars) do
				local pos = map:tileIndicesToWorldCoords{i = ee.x, j = ee.y}
				love.graphics.circle("fill", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, ee.r * globals.unitScale.x, 25)
				love.graphics.circle("fill", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, 8, 25)
			end
			-- Draw rect stuff.
			if solver.rectList then
				-- Draw rect bounds.
				for _, rect in ipairs(solver.rectList) do
					love.graphics.setLineWidth(4)
					love.graphics.setColor(50, 240, 50, 255)
					local pos = map:tileIndicesToWorldCoords{i = rect.x - 0.5, j = rect.y - 0.5}
					love.graphics.rectangle("line", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, rect.width * globals.unitScale.x, rect.height * globals.unitScale.y)
				end
				-- Draw rect connections (color coded: E:red, N:yellow-ish, W:cyan, S:purple-y).
				for _, rect in ipairs(solver.rectList) do
					for _, face in pairs(rect.connections) do
						for _, connection in ipairs(face) do
							if connection.orientation == mapSolver.orientation.EAST then
								love.graphics.setColor(255, 64, 64, 255)
							elseif connection.orientation == mapSolver.orientation.NORTH then
								love.graphics.setColor(145, 255, 50, 255)
							elseif connection.orientation == mapSolver.orientation.WEST then
								love.graphics.setColor(64, 192, 192, 255)
							else -- SOUTH
								love.graphics.setColor(128, 64, 255, 255)
							end
							local pos = map:tileIndicesToWorldCoords{i = rect.x + connection.i - 1.5, j = rect.y + connection.j - 1.5}
							love.graphics.rectangle("line", pos.x * globals.unitScale.x, pos.y * globals.unitScale.y, connection.width * globals.unitScale.x, connection.height * globals.unitScale.y)
						end
					end
				end
			end
		end

		--> DISABLED TEMP since there is no world.entities anymore.
		-- --> Render sprite bounds!
		-- if self.renderSpriteBounds then
			-- love.graphics.setColor(255, 100, 0, 255)
			-- love.graphics.setLineWidth(3)
			-- for _, e in ipairs(world.entities) do
				-- if e.drawLayer and e.sprite and e.position and e.size then
					-- local x = e.position.x * globals.unitScale.x
					-- local y = e.position.y * globals.unitScale.y
					-- local w = e.size.x * globals.unitScale.x
					-- local h = e.size.y * globals.unitScale.y
					-- love.graphics.rectangle("line", x - 0.5 * w, y - 0.5 * h, w, h)
				-- end
			-- end
		-- end
	end

end

function debugging:trigger(key)

	if key == "return" then
		if love.keyboard.isDown("lctrl") and love.keyboard.isDown("lalt") then
			self:toggle()
		end
	end

	if self.active then

		--> In debug mode, quit the game with "q" or "esc"!
		if (key == "q" or key == "escape") then
			love.event.quit()
		elseif key == "f" then
			self.renderGrid = (self.renderGrid + 1) % 3
		elseif key == "r" then
			self.redrawMap = true
		elseif key == "p" then
			self.renderPath = not self.renderPath
		elseif key == "z" then
			self.renderSpriteBounds = not self.renderSpriteBounds
		end

	end

end

return debugging
