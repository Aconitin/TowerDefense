--> This file contains some simple wrappers for all entities. Note that these are just helpers, you can create new entities (=bags of components) on the fly as you with!

--> Load all components!
local target = hooECS.Component.load({"target"})
local flyTowards = hooECS.Component.load({"flyTowards"})
local position = hooECS.Component.load({"position"})
local speed = hooECS.Component.load({"speed"})
local removeable = hooECS.Component.load({"removeable"})

local color = hooECS.Component.load({"color"})
local direction = hooECS.Component.load({"direction"})
local orientation = hooECS.Component.load({"orientation"})
local velocity = hooECS.Component.load({"velocity"})
local sprite = hooECS.Component.load({"sprite"})
local health = hooECS.Component.load({"health"})
local scale = hooECS.Component.load({"scale"})
local path = hooECS.Component.load({"path"})
local isTotem = hooECS.Component.load({"isTotem"})
local size = hooECS.Component.load({"size"})
local isCursor = hooECS.Component.load({"isCursor"})
local isEnemy = hooECS.Component.load({"isEnemy"})
local isTower = hooECS.Component.load({"isTower"})
local isBullet = hooECS.Component.load({"isBullet"})
local range = hooECS.Component.load({"range"})
local countdown = hooECS.Component.load({"countdown"})

local entities = {}

--> Test entity!
function entities.pig(x, y)

	local pig = hooECS.Entity()

	pig		:add(color())
			:add(direction())
			:add(orientation())
			:add(scale())
			:add(velocity())
			:add(sprite(resources.pig))
			:add(health())
			:add(position(x, y))

	return pig

end

--> Basic enemy!
function entities.walker(x, y)

	local walker = hooECS.Entity()

	walker	:add(color())
			:add(direction())
			:add(orientation())
			:add(scale())
			:add(velocity())
			:add(sprite(resources.walker))
			:add(health())
			:add(position(x, y))
			:add(path())
			:add(speed())
			:add(isEnemy())
			:add(removeable())
			
	return walker

end

--> Basic totem!
function entities.totem(x, y, r, g, b)

	local totem = hooECS.Entity()

	totem	:add(color(r, g, b))
			:add(orientation())
			:add(scale(0.5, 0.5))
			:add(sprite(resources.totem))
			:add(position(x, y))
			:add(isTotem())

	return totem

end

--> Basic tower!
function entities.tower(x, y)

	local tower = hooECS.Entity()

	tower	:add(color(r, g, b))
			:add(orientation())
			:add(scale())
			:add(sprite(resources.tower))
			:add(position(x, y))
			:add(size({width = 1, height = 1}))
			:add(isTower())
			:add(target())
			:add(range())
			:add(countdown())

	return tower

end

--> Basic bullet!
function entities.bullet(x, y, t)

	local bullet = hooECS.Entity()
	local function onArrive(bullet)
		local target = bullet:get("target").target
		target:get("health"):doDamage(0.32)
		bullet:get("removeable"):remove()
	end

	bullet	:add(color(r, g, b))
			:add(orientation())
			:add(direction())
			:add(scale())
			:add(sprite(resources.bullet))
			:add(position(x, y))
			:add(isBullet())
			:add(target(t))
			:add(speed(15))
			:add(removeable())
			:add(flyTowards(-100, -100, onArrive, true, true))

	return bullet

end

--> The actual mouse cursor!
function entities.cursor()

	local cursor = hooECS.Entity()

	cursor	:add(isCursor())

	return cursor
	
end

return entities
