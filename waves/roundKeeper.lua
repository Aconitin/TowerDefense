local function roundKeeper()

	local rk = {}
	local dp = true --> NOT rk.dp!
	rk.oracle = require("waves.oracle")(rk) --> The AI, heart and soul of the wave system!
	
	--> Variables for building waves!
	rk.coWaveBuilder = nil --> Holds the coroutine for wave building!
	rk.maxWaves = 1 --> Creates waves until this maximum is met!
	rk.waves = {} --> Holds all waves in the order they will get triggerd in!
	
	--> Variables to keep track of current wavestates!
	rk.gameTime = 0 --> Current gametime!
	rk.timeStampLastWavestart = 0
	
	--> Update callback!
	function rk:update(engine, map, dt)

		--> Increase gametime!
		self.gameTime = self.gameTime + dt
	
		--> Check on, create or resume the wave builder coroutine!
		if not self.coWaveBuilder then
			if self:wantNewWave() then
				if dp then print("Creating new wave builder coroutine!") end
				self.coWaveBuilder = coroutine.create(self.waveBuilder)
			end
		else --> Note that starting a new coroutine only happens in the next frame in .resume()!
			local status = coroutine.status(self.coWaveBuilder)
			if status == "dead" then
				if dp then print("Wave builder found dead, deleting reference!") end
				self.coWaveBuilder = nil
			elseif status == "suspended" then
				if dp then print("Wave builder resumed!") end
				coroutine.resume(self.coWaveBuilder, self)
			end
		end
			
		--> Process waves!
		for _,w in pairs(self.waves) do

			--> Trigger wave!
			if not w.triggered and w.waveStart <= self.gameTime then
				w.triggered = true
				if dp then print("Triggered new wave!") end
			end
			
			--> Process quantumWaves!
			if w.triggered then
				for i=#w.qws,1,-1 do
					local qw = w.qws[i]
				
					--> Remove quantumWave if amount reaches zero!
					if qw.amount <= 0 then
						w.qws[i] = nil
						if dp then print("Removed a quantum wave!") end
					else
					
						--> Count down start delay!
						if qw.startDelay > 0 then
							qw.startDelay = qw.startDelay - dt
						else --> Dount down interdelay
							if qw.interDelayCountdown > 0 then
								qw.interDelayCountdown = qw.interDelayCountdown - dt
							else
								--> Spawn enemy!
								qw.interDelayCountdown = qw.interDelay
								qw.amount = qw.amount - 1
								self:spawn(engine, map, qw.enemyType)
							end
						end
					end
				end
			end
		end
	end
	
	--> This coroutine keeps creating waves!
	function rk.waveBuilder(self) --> Self refers to rk!
		
		--> Create new wave object!
		local nw = {}
		coroutine.yield()
			
		--> Wave object variables!
		nw.qws = {} --> Quantum waves!
		nw.waveStart = self.oracle:query("waveStart") or 5
		nw.triggered = false
		coroutine.yield()
		
		--> Now add quantum waves!
		local n = self.oracle:query("qwsNr") or 1
		for i=1,n,1 do
			local enemyType = self.oracle:query("enemyType") or "walker"
			local amount = self.oracle:query("enemyAmount") or 1
			local interDelay = self.oracle:query("interDelay") or 1
			local startDelay = self.oracle:query("startDelay") or 2
			table.insert(nw.qws, self:newQuantumWave({enemyType = enemyType, amount = amount, interDelay = interDelay, startDelay = startDelay}))
			coroutine.yield()
		end
		
		table.insert(self.waves, nw)
		
	end
	
	--> Returns true if all conditions to create a new wave are met, false otherwise!
	function rk:wantNewWave()
		return #self.waves < self.maxWaves
	
	end
	
	--> Creates and returns a smallest possible sub-wave!
	function rk:newQuantumWave(p)

		local w = {}
		local p = p or {}
		
		assert(p.enemyType, "Error: no enemy type in wave.lua parameter!")

		--> Parameters!
		w.enemyType = p.enemyType --> Type of enemy in this qWave!
		w.amount = p.amount or 0 --> Amount of enemies!
		w.interDelay = p.interDelay or 0 --> Delay between each enemy's spawn!
		w.interDelayCountdown = 0
		w.startDelay = p.startDelay or 0 --> Delay before first enemy's spawn relative to wave start!

		if dp then print("Created new Quantum wave!") end
		return w
	end
	
	--> Spawns a single enemy!
	function rk:spawn(engine, map, e)
	
		--> Pick random map entrance!
		local re = map.entrances[love.math.random(1, #map.entrances)]
		local x = re.x - 1.5 + math.rsign() * math.prandom(0, re.r) --> Minus 1.5 due to weird tile enumeration!
		local y = re.y - 1.5 + math.rsign() * math.prandom(0, re.r)
		engine:addEntity(entities[e](x, y))
		if dp then print("Spawned a " .. e .. " at x|y = " .. x .. "|" .. y) end
	end
	
	--> Getters!
	function rk:getGameTime() return rk.gameTime end
	function rk:getWavesCount() return #rk.waves end
	
	--> Return roundKeeper instance!
	return rk

end
	
return roundKeeper