local function newOracle(roundKeeperReference) --> Gives back parameters data based on ... stuff. On stuff, yeah!

	local oracle = {}
	oracle.rk = roundKeeperReference
		
	function oracle:query(p) --> p is the parameter you want to know!
	
		--> Timestamp (see rk.gameTime) on when exactly this wave will begin!
		if p == "waveStart" then
			self.rk.timeStampLastWavestart = self.rk.timeStampLastWavestart + 2
			return self.rk.timeStampLastWavestart
		
		--> How many subwaves this wave will have!
		elseif p == "qwsNr" then
			return 1
		
		--> What the enemy type of the current subwave should be!
		elseif p == "enemyType" then
		
		--> How many of that enemy type there should be in that current subwave!
		elseif p == "enemyAmount" then
			return 1
		--> The time between each individual enemy in that subwave!
		elseif p == "interDelay" then

		--> The time that is waited before this subwave begins!
		elseif p == "startDelay" then
		
		end
	
	end
	
	return oracle	

end
	
return newOracle