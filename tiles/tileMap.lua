-- a tileMap is a map made of tiles. It contains a rectangle of tiles and takes care
-- of rendering them using the marching squares algorithm.

local tileMap = {}

tileMap.tileFlags = { none = 0, entrance = 1, altar = 2 }

-- Create a new tile map. Needs a tileData instance (a list of tile "archetypes"),
-- a mapping instance (translates groups of tiles into the appropriate graphic
-- representation), and a tiny-ecs world instance (takes care of entity management).
function tileMap.new(tileData, mapping, width, height)
	width = width or 48
	height = height or 48

	local spriteBatch = love.graphics.newSpriteBatch(mapping.texture, 10000)

	local output = {
		tileData = tileData, mapping = mapping,
		width = width, height = height,
		spriteBatch = spriteBatch,
		tiles = {},
		entrances = {},
		altars = {},
		playfield = {x = 1, y = 1, width = width, height = height} -- Set this to limit camera / playing field.
	}
	setmetatable(output, {__index = tileMap})
	return output
end

function tileMap:render()
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(self.spriteBatch)
end

-- Return true if (i,j) is within bounds, false otherwise.
function tileMap:checkBounds(i, j)
	return i >= 1 and i <= self.width and j >= 1 and j <= self.height
end

-- Add a tile with archetype corresponding to dataID in the grid position i, j.
-- Warning: if filling the map, it's better to do it in ascending order to ensure
-- proper array behaviour (one row at a time).
function tileMap:newTile(type, i, j)
	if self:checkBounds(i, j) then
		self.tiles[self.width * (j - 1) + i] = self.tileData[type]
	end
end

-- Read a tile, you ignorant dick.
function tileMap:get(i, j)
	if self:checkBounds(i, j) then
		return self.tiles[self.width * (j - 1) + i]
	end
	return nil
end

-- Get tile's neighbours
function tileMap:getNeighbours(i,j)
	local output = {}
	local entries = {{-1, 0}, {0, -1}, {1, 0}, {0, 1}}
	for _, index in ipairs(entries) do
		local neighbour = self:get(i + index[1], j + index[2])
		if neighbour and neighbour.passable then
			output[#output + 1] = {i = i + index[1], j = j + index[2]}
		end
	end
	return output
end

-- Remove all tiles from the ecs manager and then empty the lists.
function tileMap:empty()
	self.tiles = {}
	self.entrances = {}
	self.altars = {}
	self.spriteBatch:clear()
end

function tileMap:reset(width, height)
	self:empty()
	self.width = width
	self.height = height
end

-- Fill the spriteBatch object with sprite representations of the tiles (one sprite
-- corresponds to a corner of four tiles, see Marching Squares).
-- @Parameters rng: "mapping" uses a random number generator to choose from multiple tiles.
function tileMap:marchingSquares(rng)

	local texSize = self.mapping.tileSize
	local w, h = self.mapping.texture:getWidth(), self.mapping.texture:getHeight()

	for j = 1, self.height - 1 do
		for i = 1, self.width - 1 do

			local NW = self:get(i + 0, j + 0).id
			local NE = self:get(i + 1, j + 0).id
			local SE = self:get(i + 1, j + 1).id
			local SW = self:get(i + 0, j + 1).id

			local quad = self.mapping:getEntry(NW, NE, SE, SW, rng)
			local x, y = (i - 0.5) * globals.unitScale.x, (j - 0.5) * globals.unitScale.y
			local sx = globals.unitScale.x / self.mapping.tileSize
			local sy = globals.unitScale.y / self.mapping.tileSize

			self.spriteBatch:add(quad, x, y, 0, sx, sy)
		end
	end
end

function tileMap:convert(dir, a, b)

--[[ Terminology:

	ti = tile indices = i, j = The indices of the actual rendered tiles (not logic tiles) ranging from 0 to the tile count in both directions IN DISCRETE INTEGER STEPS!
	pc = pixel coordinates = x, y = Covering 32 units per tile, this means that the tile at index 0,0 ranges in [0, 31] for both x and y here. Map starts at 0,0!
	wc = world coordinates = x, y = Same as pixel coordinates except it ranges exactly 1 unit per tile. This is the coordinate all units operate in usually, except at render time!
	sc = screen coordinates = x, y Same as pixel coordinates except that it only covers things on screen and ranges from 0 to screen height and width. Basically before camera conversion!
	
	All directions are given as from -> to! E.g. pcti will convert from pixel coordinates to world coordinates!

--]]

	if dir == "pcti" then --> World coordinates to tile indices. Reduces any coordinate to the index of the tile that coordinate is located in!
		
		local i = math.floor((a / globals.unitScale.x))
		local j = math.floor((b / globals.unitScale.y))
		return i, j
		
	elseif dir == "tipc" then --> Tile indices to world coordinates. Gives the upper left corner of the tile as coordinate!
	
		local x = (a) * globals.unitScale.x
		local y = (b) * globals.unitScale.y
		return x, y
	
	end

end

function tileMap:tileCoordsToWorldCoords(i, j)
--print("FUNCTION tilemap.lua LINE 139 DEPRECATED PLEASE DO NOT USE ANYMORE, use :convert instead!")
	local x = self.position.x + self.scale.x * (i - 1)
	local y = self.position.y + self.scale.y * (j - 1)
	return x, y
end

--> Converts a table of tile coords into a table of tile indices!
function tileMap:tileCoordsToTileIndices(t)
--print("FUNCTION tilemap.lua LINE 147 DEPRECATED PLEASE DO NOT USE ANYMORE, use :convert instead!")
	return {i = math.round(t.x+0.5), j = math.round(t.y+0.5)}
end

--> Converts tile indices table to world coords table!
function tileMap:tileIndicesToWorldCoords(t)
--print("FUNCTION tilemap.lua LINE 153 DEPRECATED PLEASE DO NOT USE ANYMORE, use :convert instead!")
	return({x = (t.i - 0.5), y = (t.j - 0.5)})
end

--> Converts tile indices table to world coords table!
function tileMap:tileIndicesToGlobalCoords(t)
--print("FUNCTION tilemap.lua LINE 159 DEPRECATED PLEASE DO NOT USE ANYMORE, use :convert instead!")
	return({x = (t.i - 0.5) * globals.unitScale.x, y = (t.j - 0.5) * globals.unitScale.y})
end

return tileMap
