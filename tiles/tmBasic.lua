-- tmBasic.mapping is a (beautiful and) basic 16x16px outdoors tileset.
local tmBasic =  {}

tmBasic.tileData = tileData.new()

local WALL = tmBasic.tileData:add(false, {r = 100, g = 255, b = 150})
local FLOOR = tmBasic.tileData:add(true, {r = 200, g = 80, b = 120})
local ENTRANCE = tmBasic.tileData:add(true, {r = 150, g = 80, b = 170})
local ALTAR = tmBasic.tileData:add(true, {r = 150, g = 80, b = 120})
local WATER = tmBasic.tileData:add(true, {r = 70, g = 90, b = 200})

tmBasic.mapping = mapping.newMap(resources.tileset_basic, 16, 1)

--=========================--
-- DEFAULT TILE FOR ERRORS --
--=========================--

tmBasic.mapping:addErrorEntry(18, 2)

--==========================--
--   WALL AND FLOOR TILES   --
--==========================--

-- NW, NE, SE, SW
-- WALL TILE VARIATIONS
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 1)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 2)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 3)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 4)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 5)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 6)
tmBasic.mapping:addEntry(WALL, WALL, WALL, WALL, 7, 7, 20)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(WALL, WALL, WALL, FLOOR, 6, 1)
tmBasic.mapping:addEntry(WALL, WALL, FLOOR, WALL, 1, 1)
-- WALL TO THE NORTH VARIATIONS
tmBasic.mapping:addEntry(WALL, WALL, FLOOR, FLOOR, 2, 1)
tmBasic.mapping:addEntry(WALL, WALL, FLOOR, FLOOR, 3, 1)
tmBasic.mapping:addEntry(WALL, WALL, FLOOR, FLOOR, 4, 1)
tmBasic.mapping:addEntry(WALL, WALL, FLOOR, FLOOR, 5, 1)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(WALL, FLOOR, WALL, WALL, 1, 6)
tmBasic.mapping:addEntry(WALL, FLOOR, WALL, FLOOR, 3, 5)
-- WALL TO THE WEST VARIATIONS
tmBasic.mapping:addEntry(WALL, FLOOR, FLOOR, WALL, 1, 2)
tmBasic.mapping:addEntry(WALL, FLOOR, FLOOR, WALL, 1, 3)
tmBasic.mapping:addEntry(WALL, FLOOR, FLOOR, WALL, 1, 4)
tmBasic.mapping:addEntry(WALL, FLOOR, FLOOR, WALL, 1, 5)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(WALL, FLOOR, FLOOR, FLOOR, 3, 3)
tmBasic.mapping:addEntry(FLOOR, WALL, WALL, WALL, 6, 6)
-- WALL TO THE EAST VARIATIONS
tmBasic.mapping:addEntry(FLOOR, WALL, WALL, FLOOR, 6, 2)
tmBasic.mapping:addEntry(FLOOR, WALL, WALL, FLOOR, 6, 3)
tmBasic.mapping:addEntry(FLOOR, WALL, WALL, FLOOR, 6, 4)
tmBasic.mapping:addEntry(FLOOR, WALL, WALL, FLOOR, 6, 5)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, WALL, FLOOR, WALL, 2, 5)
tmBasic.mapping:addEntry(FLOOR, WALL, FLOOR, FLOOR, 2, 3)
-- WALL TO THE SOUTH VARIATIONS
tmBasic.mapping:addEntry(FLOOR, FLOOR, WALL, WALL, 2, 6)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WALL, WALL, 3, 6)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WALL, WALL, 4, 6)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WALL, WALL, 5, 6)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, FLOOR, WALL, FLOOR, 2, 2)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, WALL, 3, 2)
-- FLOOR TILE VARIATIONS
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 4, 5)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 4, 4)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 4, 3)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 4, 2)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 5, 5, 20)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 5, 4)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 5, 3)
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, FLOOR, 5, 2)

--==========================--
--   WATER TO FLOOR TILES   --
--==========================--

-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, WATER, 13, 7)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WATER, FLOOR, 8, 7)
-- FLOOR TO THE NORTH VARIATIONS
tmBasic.mapping:addEntry(FLOOR, FLOOR, WATER, WATER, 9, 7)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WATER, WATER, 10, 7)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WATER, WATER, 11, 7)
tmBasic.mapping:addEntry(FLOOR, FLOOR, WATER, WATER, 12, 7)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, WATER, FLOOR, FLOOR, 8, 12)
tmBasic.mapping:addEntry(FLOOR, WATER, FLOOR, WATER, 10, 11)
-- FLOOR TO THE WEST VARIATIONS
tmBasic.mapping:addEntry(FLOOR, WATER, WATER, FLOOR, 8, 8)
tmBasic.mapping:addEntry(FLOOR, WATER, WATER, FLOOR, 8, 9)
tmBasic.mapping:addEntry(FLOOR, WATER, WATER, FLOOR, 8, 10)
tmBasic.mapping:addEntry(FLOOR, WATER, WATER, FLOOR, 8, 11)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, WATER, WATER, WATER, 10, 9)
tmBasic.mapping:addEntry(WATER, FLOOR, FLOOR, FLOOR, 13, 12)
-- FLOOR TO THE EAST VARIATIONS
tmBasic.mapping:addEntry(WATER, FLOOR, FLOOR, WATER, 13, 8)
tmBasic.mapping:addEntry(WATER, FLOOR, FLOOR, WATER, 13, 9)
tmBasic.mapping:addEntry(WATER, FLOOR, FLOOR, WATER, 13, 10)
tmBasic.mapping:addEntry(WATER, FLOOR, FLOOR, WATER, 13, 11)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(WATER, FLOOR, WATER, FLOOR, 9, 11)
tmBasic.mapping:addEntry(WATER, FLOOR, WATER, WATER, 9, 9)
-- FLOOR TO THE SOUTH VARIATIONS
tmBasic.mapping:addEntry(WATER, WATER, FLOOR, FLOOR, 9, 12)
tmBasic.mapping:addEntry(WATER, WATER, FLOOR, FLOOR, 10, 12)
tmBasic.mapping:addEntry(WATER, WATER, FLOOR, FLOOR, 11, 12)
tmBasic.mapping:addEntry(WATER, WATER, FLOOR, FLOOR, 12, 12)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(WATER, WATER, FLOOR, WATER, 9, 8)
tmBasic.mapping:addEntry(WATER, WATER, WATER, FLOOR, 10, 8)
-- WATER TILE VARIATIONS
tmBasic.mapping:addEntry(WATER, WATER, WATER, WATER, 12, 11)

--===============================================--
-- ENTRANCE TILES (USING PIT GRAPHICS FOR NOW) --
--===============================================--

-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, ENTRANCE, 13, 13)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ENTRANCE, FLOOR, 8, 13)
-- FLOOR TO THE NORTH VARIATIONS
tmBasic.mapping:addEntry(FLOOR, FLOOR, ENTRANCE, ENTRANCE, 9, 13)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ENTRANCE, ENTRANCE, 10, 13)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ENTRANCE, ENTRANCE, 11, 13)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ENTRANCE, ENTRANCE, 12, 13)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, FLOOR, FLOOR, 8, 18)
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, FLOOR, ENTRANCE, 10, 17)
-- FLOOR TO THE WEST VARIATIONS
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, ENTRANCE, FLOOR, 8, 14)
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, ENTRANCE, FLOOR, 8, 15)
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, ENTRANCE, FLOOR, 8, 16)
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, ENTRANCE, FLOOR, 8, 17)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, ENTRANCE, ENTRANCE, ENTRANCE, 10, 15)
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, FLOOR, FLOOR, 13, 18)
-- FLOOR TO THE EAST VARIATIONS
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, FLOOR, ENTRANCE, 13, 14)
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, FLOOR, ENTRANCE, 13, 15)
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, FLOOR, ENTRANCE, 13, 16)
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, FLOOR, ENTRANCE, 13, 17)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, ENTRANCE, FLOOR, 9, 17)
tmBasic.mapping:addEntry(ENTRANCE, FLOOR, ENTRANCE, ENTRANCE, 9, 15)
-- FLOOR TO THE SOUTH VARIATIONS
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, FLOOR, FLOOR, 9, 18)
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, FLOOR, FLOOR, 10, 18)
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, FLOOR, FLOOR, 11, 18)
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, FLOOR, FLOOR, 12, 18)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, FLOOR, ENTRANCE, 9, 14)
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, ENTRANCE, FLOOR, 10, 14)
-- ENTRANCE TILE VARIATIONS
tmBasic.mapping:addEntry(ENTRANCE, ENTRANCE, ENTRANCE, ENTRANCE, 12, 14)

--==================================================--
-- ALTAR TILES (USING DEEP GROUND GRAPHICS FOR NOW) --
--==================================================--

-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, FLOOR, FLOOR, ALTAR, 13, 1)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ALTAR, FLOOR, 8, 1)
-- FLOOR TO THE NORTH VARIATIONS
tmBasic.mapping:addEntry(FLOOR, FLOOR, ALTAR, ALTAR, 9, 1)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ALTAR, ALTAR, 10, 1)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ALTAR, ALTAR, 11, 1)
tmBasic.mapping:addEntry(FLOOR, FLOOR, ALTAR, ALTAR, 12, 1)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, ALTAR, FLOOR, FLOOR, 8, 6)
tmBasic.mapping:addEntry(FLOOR, ALTAR, FLOOR, ALTAR, 10, 5)
-- FLOOR TO THE WEST VARIATIONS
tmBasic.mapping:addEntry(FLOOR, ALTAR, ALTAR, FLOOR, 8, 2)
tmBasic.mapping:addEntry(FLOOR, ALTAR, ALTAR, FLOOR, 8, 3)
tmBasic.mapping:addEntry(FLOOR, ALTAR, ALTAR, FLOOR, 8, 4)
tmBasic.mapping:addEntry(FLOOR, ALTAR, ALTAR, FLOOR, 8, 5)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(FLOOR, ALTAR, ALTAR, ALTAR, 10, 3)
tmBasic.mapping:addEntry(ALTAR, FLOOR, FLOOR, FLOOR, 13, 6)
-- FLOOR TO THE EAST VARIATIONS
tmBasic.mapping:addEntry(ALTAR, FLOOR, FLOOR, ALTAR, 13, 2)
tmBasic.mapping:addEntry(ALTAR, FLOOR, FLOOR, ALTAR, 13, 3)
tmBasic.mapping:addEntry(ALTAR, FLOOR, FLOOR, ALTAR, 13, 4)
tmBasic.mapping:addEntry(ALTAR, FLOOR, FLOOR, ALTAR, 13, 5)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(ALTAR, FLOOR, ALTAR, FLOOR, 9, 5)
tmBasic.mapping:addEntry(ALTAR, FLOOR, ALTAR, ALTAR, 9, 3)
-- FLOOR TO THE SOUTH VARIATIONS
tmBasic.mapping:addEntry(ALTAR, ALTAR, FLOOR, FLOOR, 9, 6)
tmBasic.mapping:addEntry(ALTAR, ALTAR, FLOOR, FLOOR, 10, 6)
tmBasic.mapping:addEntry(ALTAR, ALTAR, FLOOR, FLOOR, 11, 6)
tmBasic.mapping:addEntry(ALTAR, ALTAR, FLOOR, FLOOR, 12, 6)
-- INTERMEDIATE TILES
tmBasic.mapping:addEntry(ALTAR, ALTAR, FLOOR, ALTAR, 9, 2)
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, FLOOR, 10, 2)
-- ALTAR TILE VARIATIONS
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, ALTAR, 11, 5)
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, ALTAR, 11, 4)
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, ALTAR, 11, 2, 20)
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, ALTAR, 12, 5)
tmBasic.mapping:addEntry(ALTAR, ALTAR, ALTAR, ALTAR, 12, 4)

return tmBasic
