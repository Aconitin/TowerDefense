prepend = (...):gsub('%.[^%.]+$', '') .. "."
local basic = require(prepend .. "mapSolver.mapSolver_basic")
local tileTypes = basic.tileTypes
local orientation = basic.orientation
local rect = require(prepend .. "mapSolver.mapSolver_rect")
local patterns = require(prepend .. "mapSolver_patterns")
local graph = require(prepend .. "mapSolver_graph")

local mapSolver = {tileTypes = tileTypes, orientation = orientation, rect = rect, patterns = patterns, graph = graph}

-- MAIN MACHINERY! Create a traversable map and such.

-- Creates new tiles for tileMap from the tile types encoded in list (a list of rects). Starts from (1,1) (upper corner), fills undefined tiles with defaultTile.
function mapSolver.copyListToTilemap(list, playfieldPadding, outerPadding, tileData, mapping, defaultTile)
	local coalesce = {}
	local totalPadding = playfieldPadding + outerPadding
	local width, height = rect.position(list, totalPadding)
	local tileMap = tileMap.new(tileData, mapping, width, height)
	tileMap.playfield.x = outerPadding + 1
	tileMap.playfield.y = outerPadding + 1
	tileMap.playfield.width  = width  - 2 * outerPadding
	tileMap.playfield.height = height - 2 * outerPadding

	for y = 1, height do
		for x = 1, width do
			coalesce[(y - 1) * width + x] = tileTypes.IGNORE
		end
	end

	for _, rect in ipairs(list) do
		for y = 1, height do
			for x = 1, width do
				local type = rect:getFromCoords(x, y)
				if tileTypes.isFinal(type) then
					coalesce[(y - 1) * width + x] = type
				end
			end
		end
		for _, e in ipairs(rect.entrances) do
			tileMap.entrances[#(tileMap.entrances) + 1] = {x = e.x + rect.x, y = e.y + rect.y, r = e.r}
		end
		for _, a in ipairs(rect.altars) do
			tileMap.altars[#(tileMap.altars) + 1] = {x = a.x + rect.x, y = a.y + rect.y, r = a.r}
		end
	end

	for y = 1, height do
		for x = 1, width do
			local type = coalesce[(y - 1) * width + x]
			type = tileTypes.isFinal(type) and type or defaultTile
			tileMap:newTile(type, x, y)
		end
	end

	return tileMap
end

-- Returns a new mapSolver object.
function mapSolver.new(tileData, mapping, seed)
		local rng = seed and random.new(seed) or random.new(love.timer.getTime())
		local output = setmetatable({tileData = tileData, mapping = mapping, rng = rng, totems = {}}, {__index = mapSolver})
		return output
end

-- TODO: THIS SHOULD BE INTELLIGENT; DUMB AS FUCK RIGHT NOW
-- Invokes the cosmic forces of AI and bad programming to create a new map.
-- Returns:
--   - rect: rectangle with the final map dimensions and tile data.
--   - list: list of rectangles used for building rect; returned for debug purposes.
function mapSolver:makeMapList()
	local list = {}

	-- local entranceLeft = patterns.entrance(orientation.EAST)
	-- local entranceRight = patterns.entrance(orientation.WEST)
	-- local zigZagConfigLeft  = {width = self.rng:uniformInt(25, 50), height = self.rng:uniformInt(20, 40), minPathWidth = 2, maxPathWidth = 4, minWallWidth = 2, maxWallWidth = 4, minPadX = 1, maxPadX = 2, minPadY = 1, maxPadY = 8}
	-- local zigZagConfigRight = {width = self.rng:uniformInt(25, 50), height = self.rng:uniformInt(20, 40), minPathWidth = 2, maxPathWidth = 4, minWallWidth = 2, maxWallWidth = 4, minPadX = 1, maxPadX = 2, minPadY = 1, maxPadY = 8}
	-- local zigZagLeft = patterns.zigZagHorizontal(self, zigZagConfigLeft)
	-- local zigZagRight = patterns.zigZagHorizontal(self, zigZagConfigRight)
	-- local crossing = patterns.cross(3, self.rng:uniformInt(2, 5), true, false, true, true)
	-- local fatPath = patterns.path(self.rng:uniformInt(10, 25), false, 3, self.rng:uniformInt(1, 3))
	-- local altar = patterns.altar(orientation.NORTH)
  --
	-- local list = {crossing, zigZagRight, zigZagLeft, entranceRight, entranceLeft, fatPath, altar}
  --
	-- crossing.x, crossing.y = 0, 0
	-- zigZagRight:connectTo(1, crossing, 1)
	-- entranceRight:connectTo(1, zigZagRight, 2)
	-- zigZagLeft:connectTo(2, crossing, 2)
	-- entranceLeft:connectTo(1, zigZagLeft, 1)
	-- fatPath:connectTo(1, crossing, 3)
	-- altar:connectTo(1, fatPath, 2)

	------------------------------------------------------------------------------

	local numEntrances = self.rng:uniformInt(1, 3)
	local numAltars = self.rng:uniformInt(3, 5)
	local pathLength = self.rng:uniformInt(24, 38)
	local fatten = self.rng:uniformInt(6, 10)


	local entrancesComb = patterns.comb(numEntrances, 3, 2, orientation.EAST)
	list[1] = entrancesComb

	for i = 1, numEntrances do
		local entrance = patterns.entrance(orientation.EAST)
		list[#list + 1] = entrance
		entrance:connectTo(orientation.EAST, entrancesComb)
	end

	local fatPath = patterns.path(pathLength, true, 3, fatten)
	fatPath:connectTo(orientation.WEST, entrancesComb)
	list[#list + 1] = fatPath

	local altarsComb = patterns.comb(numAltars, 3, 2, orientation.WEST)
	list[#list + 1] = altarsComb
	altarsComb:connectTo(orientation.WEST, fatPath)

	for i = 1, numAltars do
		local altarOffset = (i > numAltars / 2) and 1 or 0
		local altar = patterns.altar(orientation.WEST)
		list[#list + 1] = altar
		altar:connectTo(orientation.WEST, altarsComb)
	end

	------------------------------------------------------------------------------

	-- list[1] = patterns.ballCrossing(10, 3, 23, true, true, true, true)

	return list
end

-- TODO: REVISE HOW THIS SHOULD OPERATE. COPIED FROM OLD tileMap.
-- Populate the map with its starting entities.
-- TODO: RIGHT NOW, ONLY TOTEMS.
function mapSolver:populate()
	-- Cleanup
	for _, totem in ipairs(self.totems) do
		engine:removeEntity(totem)
	end
	self.totems = {}

	-- Fill
	local numAltars = #(self.tileMap.altars)
	local h0 = self.rng:uniformInt(0, 255)

	for i, altar in ipairs(self.tileMap.altars) do

		local r, g, b = utils.HSL((h0 + (i * 256) / numAltars) % 256, 255, 190)

		local newTotem = entities.totem(altar.x - 0.5, altar.y - 0.5, r, g, b)
		engine:addEntity(newTotem)
		self.totems[#(self.totems) + 1] = newTotem

	end

end

-- Make the magic happen! Call this from the outside to invoke the creation process.
function mapSolver:solve()
	self.rectList = self:makeMapList()
	self.tileMap = mapSolver.copyListToTilemap(self.rectList, 2, 10, self.tileData, self.mapping, tileTypes.DEFAULT)
	self.tileMap:marchingSquares(self.rng)
	self:populate()

	return tileMap
end

return mapSolver
