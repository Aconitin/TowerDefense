-- ================== --
--     DEPRECATED     --
-- ================== --

-- But still around for reference, cool stuff in here.

local mapDrawingFunctions = {}

-- local function genericMapDrawLoop(setup, loop)
-- 	return function(tm)
-- 		local width = tm.horizontalTiles
-- 		local height = tm.verticalTiles
-- 		setup(tm, width, height)
-- 		for j = 1, height do
-- 			for i = 1, width do
-- 				loop(tm, width, height, i, j)
-- 			end
-- 		end
-- 	end
-- end

function mapDrawingFunctions.testDiagonal(wall, floor)
	return function(tm)
		local width = tm.horizontalTiles
		local height = tm.verticalTiles
		for j = 1, height do
			for i = 1, width do
				local p = (i + j - 2) / (height + width - 2) -- Diagonal probability increase
				local tile = tm:newTile(tm.rng:bernoulli(p) and wall or floor, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end
	end
end

function mapDrawingFunctions.randomLine(wall, floor)
	return function(tm)
		local width = tm.horizontalTiles
		local height = tm.verticalTiles

		local j0 = tm.rng:uniformInt(height)
		local j1 = tm.rng:uniformInt(height)
		local w = tm.rng:uniformInt(1,5)

		for j = 1, height do
			for i = 1, width do
				local d = geometry2d:point_to_segment(i + 0.5, j + 0.5, 0, j0, width, j1)
				local tile = tm:newTile((d > w) and wall or floor, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end
	end
end

function mapDrawingFunctions.line(j0, j1, w, wall, floor)
	return function(tm)
		local width = tm.horizontalTiles
		local height = tm.verticalTiles
		for j = 1, height do
			for i = 1, width do
				local d = geometry2d:point_to_segment(i + 0.5, j + 0.5, 0, j0, width, j1)
				local tile = tm:newTile((d > w) and wall or floor, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end
	end
end

function mapDrawingFunctions.squareU(padX, padY, wallWidth, floorWidth, wall, floor)
	return function(tm)
		local width = tm.horizontalTiles
		local height = tm.verticalTiles

		local path = {}
		local halfFloor = floorWidth / 2
		local x = padX + halfFloor
		while x < width - padX - 2 * floorWidth - wallWidth do
			path[#path + 1] = {x = x, y = 1.5 + padY}
			path[#path + 1] = {x = x, y = height - 1.5 - padY}
			x = x + floorWidth + wallWidth
			path[#path + 1] = {x = x, y = height - 1.5 - padY}
			path[#path + 1] = {x = x, y = 1.5 + padY}
			x = x + floorWidth + wallWidth
		end

		for j = 1, height do
			for i = 1, width do
				local d = geometry2d:point_to_polyline(i - 0.5, j - 0.5, path, #path)
				local tile = tm:newTile((d > halfFloor) and wall or floor, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end

	end
end

function mapDrawingFunctions.oldSquareZigZag(padX, padY, wallWidth, floorWidth, wall, floor)
	return function(tm)
		local width = tm.horizontalTiles
		local height = tm.verticalTiles

		local halfFloor = floorWidth / 2
		local lowY = 0.5 + padY + halfFloor
		local highY = height - lowY
		local lowX = 0.5 + padX + halfFloor
		local highX = width - lowX

		local x = lowX
		local goingUp = true

		local path = {{x = 1, y = lowY}}

		while x < highX do
			path[#path + 1] = {x = x, y = goingUp and lowY or highY}
			path[#path + 1] = {x = x, y = goingUp and highY or lowY}
			x = x + floorWidth + wallWidth
			goingUp = not goingUp
		end

		path[#path + 1] = {x = width, y = goingUp and lowY or highY}

		for j = 1, height do
			for i = 1, width do
				local d = geometry2d:point_to_polyline(i - 0.5, j - 0.5, path, #path)
				local tile = tm:newTile((d > halfFloor) and wall or floor, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end

		tm.entrances[1] = {path[1]}
		tm.exits[1] = {path[#path]}
		-- print(tm.entrances)
		-- print(tm.exits)
	end
end

function mapDrawingFunctions.simplePath(pathGeneration, wall, floor, entrance, config)
	return function(tm)
		local path = pathGeneration(tm, config)
		for j = 1, tm.verticalTiles do
			for i = 1, tm.horizontalTiles do
				local tileType = wall
				for k = 1, #path - 1 do
					if path[k].r > geometry2d:point_to_segment(i - 0.5, j - 0.5, path[k].x, path[k].y, path[k + 1].x, path[k + 1].y) then
						tileType = floor
						break
					end
				end
				if geometry2d:point_to_point(i - 0.5, j - 0.5, path[1].x, path[1].y) < 1.1 then
					tileType = entrance
				end
				local tile = tm:newTile(tileType, i, j)
				-- tm.world:addEntity(tile)
				tm.tiles[#(tm.tiles) + 1] = tile
			end
		end
		tm.entrances[1] = path[1]
		tm.exits[1] = path[#path]
		tm.path = path
	end
end

local zigZagPath = function(tm, config)
	local width = tm.horizontalTiles
	local height = tm.verticalTiles

	local halfFloor = config.floorWidth / 2
	local lowY = 0.5 + config.padY + halfFloor
	local highY = height - lowY
	local lowX = 0.5 + config.padX + halfFloor
	local highX = width - lowX

	local x = lowX
	local goingUp = true

	local path = {{x = 1, y = lowY, r = halfFloor}}

	while x < highX do
		path[#path + 1] = {x = x, y = goingUp and lowY or highY, r = halfFloor}
		path[#path + 1] = {x = x, y = goingUp and highY or lowY, r = halfFloor}
		x = x + config.floorWidth + config.wallWidth
		goingUp = not goingUp
	end

	path[#path + 1] = {x = width, y = goingUp and lowY or highY, r = halfFloor}

	return path
end

function mapDrawingFunctions.squareZigZag(padX, padY, wallWidth, floorWidth, wall, floor, entrance)
	return mapDrawingFunctions.simplePath(zigZagPath, wall, floor, entrance, {padX = padX, padY = padY, wallWidth = wallWidth, floorWidth = floorWidth})
end

local function randomZigZagPath(tm, config)
	local width = tm.horizontalTiles
	local height = tm.verticalTiles

	local getR = function()
		return tm.rng:uniformInt(config.minFloorWidth, config.maxFloorWidth) / 2
	end

	local x = 0.5 + config.maxFloorWidth / 2 + tm.rng:uniformInt(config.minPadX, config.maxPadX)
	local X = width - config.maxFloorWidth / 2 - tm.rng:uniformInt(config.minPadX, config.maxPadX)
	local y = 0.5 + config.maxFloorWidth / 2 + tm.rng:uniformInt(config.minPadY, config.maxPadY)
	local Y = height - config.maxFloorWidth / 2 - tm.rng:uniformInt(config.minPadY, config.maxPadY)

	local goingUp = tm.rng:bernoulli(0.5)

	local path = {{x = x, y = goingUp and y or Y, r = config.capRadius}}
	-- local path = {}
	local nextR = getR()

	while x < X do
		path[#path + 1] = {x = x, y = goingUp and y or Y, r = nextR}
		y = 0.5 + config.maxFloorWidth / 2 + tm.rng:uniformInt(config.minPadY, config.maxPadY)
		Y = height - config.maxFloorWidth / 2 - tm.rng:uniformInt(config.minPadY, config.maxPadY)
		path[#path + 1] = {x = x, y = goingUp and Y or y, r = getR()}
		nextR = getR()
		x = x + path[#path].r + nextR + tm.rng:uniformInt(config.minWallWidth, config.maxWallWidth)
		goingUp = not goingUp
	end

	path[#path].r = config.capRadius
	path[#path + 1] = {x = path[#path].x, y = path[#path].y, r = config.capRadius}

	print(path)
	return path
end

function mapDrawingFunctions.randomZigZag(wall, floor, entrance, config)
	return mapDrawingFunctions.simplePath(randomZigZagPath, wall, floor, entrance, config)
end

local function sinusoidPath(tm, config)
	local width = tm.horizontalTiles
	local height = tm.verticalTiles

	local oscillations = tm.rng:uniformInt(2 * config.minOscillations, 2 * config.maxOscillations) / 2
	local initialPhase = tm.rng:bernoulli(0.5) and 0 or math.pi
	local padX = tm.rng:uniformInt(config.minPadX, config.maxPadX)
	local padY = tm.rng:uniformInt(config.minPadY, config.maxPadY)

end

return mapDrawingFunctions
