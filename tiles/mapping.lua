-- A tileset mapping is used in the marching squares algorithm to translate a square
-- of 4 tiles into a particular image. For example, if the two upper tiles are grass
-- tiles and the two lower tiles are dirt tiles, the image needs to be a "border
-- tile" that shows the vertical transition from grass to dirt.

local mapping = {}

function mapping.newMap(texture, tileSize, padding)
	padding = padding or 0
	local rng = seed and random.new(seed) or random.new()
	local output = setmetatable(
		{texture = texture, tileSize = tileSize, padding = padding, rng = rng, entries = {}},
		{__index = mapping}
	)
	return output
end

function mapping:makeEntryQuad(i, j)
	local paddedTileSize = self.tileSize + 2 * self.padding
	return love.graphics.newQuad(
		self.padding + (i - 1) * paddedTileSize,
		self.padding + (j - 1) * paddedTileSize,
		self.tileSize,
		self.tileSize,
		self.texture:getWidth(),
		self.texture:getHeight()
	)
end

function mapping:addEntry(nw, ne, se, sw, i, j, weight)
	weight = weight or 1
	local entry = self:makeEntryQuad(i, j)
	for k, v in pairs(self.entries) do
		if k[1] == nw and k[2] == ne and k[3] == se and k[4] == sw then
			v[entry] = weight
			return
		end
	end
	self.entries[{nw, ne, se, sw}] = {[entry] = weight}
end

function mapping:getEntry(nw, ne, se, sw, rng)
	for k, v in pairs(self.entries) do
		if k[1] == nw and k[2] == ne and k[3] == se and k[4] == sw then
			return rng:weighted(v)
		end
	end
	return self.errorEntry
end

function mapping:addErrorEntry(i, j)
	self.errorEntry = self:makeEntryQuad(i, j)
end

return mapping
