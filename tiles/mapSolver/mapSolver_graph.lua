prepend = (...):gsub('%.[^%.]+$', '') .. "."
local basic = require(prepend .. "mapSolver_basic")
local tileTypes = basic.tileTypes
local orientation = basic.orientation
local rect = require(prepend .. "mapSolver_rect")

-- The graph class takes care of choosing the sequence of patterns to use and how to connect them.

local graph = {}

--------------------------------------------------------------------------------

-- substitutionRule helper class.
-- Assumes all entries in catalog are functions that return a rating and a list of rects.
-- Uses this rating to find the best substitution to apply at any given moment.

graph.substitution = {}
local substitution = graph.substitution -- For ease of coding
substitution.catalog = {}

local function orderRatings(a, b)
  return a.rating > b.rating
end

-- TODO: UNTESTED
-- Given a rect, some generic configuration parameters and a random number generator,
-- get a list of possible substitutions ranked from best (highest rating) to worst.
-- Discards negative ratings.
function substitution.ranking(rect, config, rng)
  local output = {}
  for k, f in pairs(substitution.catalog) do
    local rating, list = f(rect, config, rng)
    if rating > 0 then
      output[#output + 1] = {rating = rating, list = list}
    end
  end
  if #output > 0 then
    table.sort(output, orderRatings)
  end
  return output
end

-- Given a list of rects, choose one of the best substitutions semi-randomly and apply it.
function substitution.substitute(list, config, rng)
  -- Choose which rect to substitute and which substitution to apply.
  local subs = {}
  local weightTable = {}
  for i, rect in ipairs(list) do
    subs[i] = substitution.ranking(rect, config, rng)
    for j, entry in ipairs(subs[i]) do
      weightTable[{i = i, j = j}] = entry.rating
    end
  end
  local indices = rng:weighted(weightTable)
  if not indices then return nil end
  -- Create the new list of rects.
  local output = {}
  for i = 1, #list do
    if i == indices.i then
      for _, rect in ipairs(subs[i][indices.j].list) do
        output[#output + 1] = rect
      end
    else
      output[#output + 1] = list[i]
    end
  end
  return output
end

-- Example of a substitution rule. Takes a rect and a config object, returns a rating (negative means bad match) and a list of substituting rects.
function substitution.catalog.splitHorizontal(rect, config, rng)
  -- TODO: USE graph.seed AS REFERENCE FOR THE MOST UP-TO-DATE INITIALIZATION BLOCK
  local minRectLength = config.minRectLength and config.minRectLength or 3
  local maxRectLength = config.maxRectLength and config.maxRectLength or 100
  local meanRectLength = config.meanRectLength and config.meanRectLength or 32

  if rect.special then return -1, {} end
  if rect.width < 2 * minRectLength then return -1, {} end

  local width = rng:uniformInt(minRectLength, math.min(maxRectLength, rect.width - minRectLength))
  local remainder = rect.width - width
  local list = {}
  local a = rect.new(width, width)
  list[1] = a
  local b = rect.new(remainder, remainder)
  list[2] = b
  a.x = rect.x
  a.y = rect.y + math.floor((rect.height - a.height) / 2)
  b.x = rect.x + a.width
  b.y = rect.y + math.floor((rect.height - b.height) / 2)
  local rating = maxRectLength - math.abs(meanRectLength - width) - math.abs(meanRectLength - remainder)
  return rating, list
end

-- Example of a substitution rule. Takes a rect and a config object, returns a rating (negative means bad match) and a list of substituting rects.
function substitution.catalog.makeCross(rect, config, rng)
  -- TODO: USE graph.seed AS REFERENCE FOR THE MOST UP-TO-DATE INITIALIZATION BLOCK
  local minRectLength = config.minRectLength and config.minRectLength or 3
  local maxRectLength = config.maxRectLength and config.maxRectLength or 100
  local meanRectLength = config.meanRectLength and config.meanRectLength or 32

  if rect.special then return -1, {} end
  if (rect.width < 3 * minRectLength) or (rect.height < 3 * minRectLength) then return -1, {} end

  local w = rng:uniformInt(minRectLength, math.min(maxRectLength, rect.width - 2 * minRectLength))
  local h = rng:uniformInt(minRectLength, math.min(maxRectLength, rect.height - 2 * minRectLength))
  local left = math.floor((rect.width - w) / 2)
  local right = rect.width - w - left
  local top = math.floor((rect.height - h) / 2)
  local bottom = rect.height - h - top
  local list = {}
  local rectLeft = rect.new(left, h)
  rectLeft.x, rectLeft.y = rect.x, rect.y + top
  list[1] = rectLeft
  local rectRight = rect.new(right, h)
  rectRight.x, rectRight.y = rect.x + left + w, rect.y + top
  list[2] = rectRight
  local rectTop = rect.new(w, top)
  rectTop.x, rectTop.y = rect.x + left, rect.y
  list[3] = rectTop
  local rectBottom = rect.new(w, bottom)
  rectBottom.x, rectBottom.y = rect.x + left, rect.y + top + h
  list[4] = rectBottom
  local rectCenter = rect.new(w, h)
  rectCenter.x, rectCenter.y = rect.x + left, rect.y + top
  list[5] = rectCenter
  local rating = maxRectLength
  -- local rating = maxRectLength - math.abs(meanRectLength - width) - math.abs(meanRectLength - remainder)
  return rating, list
end

--------------------------------------------------------------------------------

-- Create the initial seed graph, given configuration data and a random number generator.
function graph.seed(config, rng)

  -- TODO: THE NUMBER AND NATURE OF THE AVAILABLE FIELDS IS HIGHLY EXPERIMENTAL AND SHOULD BE REFINED.
  -- TODO: THE SAME IS TRUE FOR THE DEFAULT VALUES.
  config = config or {}
  local difficulty = config.difficulty and config.difficulty or 3600
  local plMin = config.minPathLength and config.minPathLength or 32
  local plMax = config.maxPathLength and config.maxPathLength or 128
  local plMean = config.averagePathLength and config.averagePathLength or 64
  local plDev = config.pathLengthDeviation and config.pathLengthDeviation or 16
  local minEntrances = config.minEntrances and config.minEntrances or 3
  local maxEntrances = config.maxEntrances and config.maxEntrances or 5
  local minAltars = config.minAltars and config.minAltars or 2
  local maxAltars = config.maxAltars and config.maxAltars or 4
  local minRectLength = config.minRectLength and config.minRectLength or 3
  local maxRectLength = config.maxRectLength and config.maxRectLength or 100
  local meanRectLength = config.meanRectLength and config.meanRectLength or 32

  local numEntrances = rng:uniformInt(minEntrances, maxEntrances)
  local numAltars = rng:uniformInt(minAltars, maxAltars)
  local pathLength = rng:uniformInt(plMin, plMax)

  output = {}

  -- TODO: HARDCODING A BASIC SEED FOR NOW. MANY SHAPES ARE INTERESTING HERE AS INPUT.

  local entranceHub = rect.new(7 * numEntrances, 7 * numEntrances)
  entranceHub.nonterminal = true -- Flag this rect as not being final (must be substituted).
  entranceHub.numEntrances = 3 -- Flag this nonterminal rect as having to generate 3 entrances when it's substituted.
  entranceHub.special = true
  output[1] = entranceHub

  local pathSeed = rect.new(pathLength, pathLength)
  pathSeed.nonterminal = true -- Flag this rect as not being final (must be substituted).
  output[2] = pathSeed

  local altarHub = rect.new(7 * numAltars, 7 * numAltars)
  altarHub.nonterminal = true -- Flag this rect as not being final (must be substituted).
  altarHub.numAltars = 3 -- Flag this nonterminal rect as having to generate 3 entrances when it's substituted.
  altarHub.special = true
  output[3] = altarHub

  entranceHub:setAdjacentCenteredTo(orientation.EAST, pathSeed)
  altarHub:setAdjacentCenteredTo(orientation.WEST, pathSeed)

  return output
end

-- Create the map as a graph of touching rectangles, given configuration data and a random number generator.
-- Returns a list of lists. Each inner list is a rectangle graph stored sequentially, each consecutive graph represents one step of the iteration process.
function graph.solve(config, rng)
  local output = {}
  local latest = graph.seed(config, rng)
  repeat
    output[#output + 1] = latest
    latest = substitution.substitute(output[#output], config, rng)
  until (not latest)
  return output
end

return graph
