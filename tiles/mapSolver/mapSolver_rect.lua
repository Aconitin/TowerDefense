prepend = (...):gsub('%.[^%.]+$', '') .. "."
local basic = require(prepend .. "mapSolver_basic")
local tileTypes = basic.tileTypes
local orientation = basic.orientation

-- The rect class allows working on subsections of a map, turning them, subdividing...
--
-- It has x,y coordinates and with,height size, but they DON'T correspond to game-world size and position.
-- Instead they are relative to other rects and are counted in tiles, so we can easily combine several rects into a bigger one.
-- The final goal is to be able to assemble a map as a list of connected rects that get written to a single tile map.
-- The system allows for overlaying different rects. That's why it includes pass-through tiles (IGNORE).

local rect = {}

--------------------------------------------------------------------------------

-- connectionList helper class
-- The connectionList class helps manage rect connections and retrieve them logically.
rect.connectionList = {}
local cl = rect.connectionList -- Shorthand

-- Each rect has an instance of this.
function cl.new()
  local output = setmetatable({}, {__index = cl})
  output[orientation.EAST] = {}
  output[orientation.NORTH] = {}
  output[orientation.WEST] = {}
  output[orientation.SOUTH] = {}
  return output
end

-- Used to sort connections N->S and W->E.
local function orderConnections(a, b)
  if a.j < b.j then return true
  elseif a.j > b.j then return false
  else return a.i < b.i end
end

-- Creates a new connection with the given information and stores it in the appropriate orientation list.
-- Orders the list after addition, so connections are always stored N->S and W->E.
function cl:add(i, j, width, height, orientation)
  self[orientation][#(self[orientation]) + 1] = {i = i, j = j, width = width, height = height, orientation = orientation, linkedTo = nil} -- TODO: just don't write linkedTo?
  table.sort(self[orientation], orderConnections)
end

-- Return a free connection in the given orientation, if possible.
function cl:getFirstFree(orientation)
  for _, connection in ipairs(self[orientation]) do
    if not connection.linkedTo then
      return connection
    end
  end
  return nil
end

--------------------------------------------------------------------------------

-- Clear the whole rect to a selected tile type (number value)
function rect:clear(type)
	for i = 1, self.width * self.height do
		self[i] = type
	end
end

-- Create a new rect.
function rect.new(width, height, clearTile, x, y)
	width  = ((not width ) or (width  < 0)) and 1 or width
	height = ((not height) or (height < 0)) and 1 or height
	clearTile = clearTile or tileTypes.IGNORE
	x = x or 0
	y = y or 0
	-- "entrances" is a complete list of all entrances in this rect; same for "altars".
	local output = setmetatable(
									{x = x, y = y, width = width, height = height, connections = cl.new(), entrances = {}, altars = {}},
									{__index = rect}
								)
	output:clear(clearTile)
	return output
end

-- Returns the tile at local tile coordinates (i,j).
function rect:getTile(i, j)
	return self[(j - 1) * self.width + i]
end

-- Returns the tile at "global" tile coordinates (x,y) if it falls within the rect, or NONE if it falls outside.
function rect:getFromCoords(x, y)
  local i, j = x - self.x + 1, y - self.y + 1
  if i < 1 or i > self.width or j < 1 or j > self.height then
    return tileTypes.NONE
  end
  return self:getTile(i, j)
end

function rect:setTile(i, j, type)
	self[(j - 1) * self.width + i] = type
end

function rect:setRegion(startI, startJ, width, height, type)

	-- Defaults (just in case)
	width = width or 1
	height = height or 1
	type = type or tileTypes.IGNORE

	-- Allow for negative widths and heights. Make sure we don't start off-limits.
	if width < 0 then
		startI, width = math.max(startI + width + 1, 1), -width
	elseif width == 0 then
		width = 1
	end

	if height < 0 then
		startJ, height = math.max(startJ + height + 1, 1), -height
	elseif height == 0 then
		height = 1
	end

	-- Make sure we don't end off-limits.
	local endI = math.min(self.width,  startI + width  - 1)
	local endJ = math.min(self.height, startJ + height - 1)

	-- Copy things a bunch
	for j = startJ, endJ do
		for i = startI, endI do
			self[(j - 1) * self.width + i] = type
		end
	end

end

-- Copy "other" rect's data into this rect (their relative position matters).
-- Optionally include x and y offsets. Non-final tiles will be ignored (NONE, IGNORE).
function rect:copy(other, offsetX, offsetY)
	offsetX = offsetX or 0
	offsetY = offsetY or 0
	local startI, startJ = other.x + offsetX, other.y + offsetY
	local J = math.min(other.height, self.height - startJ + 1)
	local I = math.min(other.width, self.width - startI + 1)
	for j = 1, J do
		for i = 1, I do
			local otherTile = other[(j - 1) * other.width + i]
			if tileTypes.isFinal(otherTile) then -- Only consider final tiles
				self[(startJ + j - 2) * self.width + (startI + i - 1)] = otherTile
			end
		end
	end

  -- TODO: WRITE AN ITERATOR FOR CONNECTIONS SO THIS DOESN'T DEPEND ON INNER IMPLEMENTATION?
	for _, face in pairs(other.connections) do
    for _, c in ipairs(face) do
      local newI = c.i + startI - 1
      local newJ = c.j + startJ - 1
  		if (c.orientation == orientation.EAST  and newI == self.width ) or
  		   (c.orientation == orientation.NORTH and newJ == 1          ) or
  	 		 (c.orientation == orientation.WEST  and newI == 1          ) or
  	 		 (c.orientation == orientation.SOUTH and newJ == self.height) then
  				 self.connections:add(newI, newJ, c.width, c.height, c.orientation)
  		end
    end
	end

	for _, e in ipairs(other.entrances) do
		self.entrances[#(self.entrances) + 1] = {x = e.x + startI, y = e.y + startJ, r = e.r}
	end
	for _, a in ipairs(other.altars) do
		self.altars[#(self.altars) + 1] = {x = a.x + startI, y = a.y + startJ, r = a.r}
	end
end

-- Return a copy of this rect, turned 90º counter clockwise.
function rect:turn()
	local output = rect.new(self.height, self.width, tileTypes.IGNORE, self.x, self.y)
	for selfJ = 1, self.height do
		local outI = selfJ
		for selfI = 1, self.width do
			local outJ = self.width - selfI + 1
			output[(outJ - 1) * output.width + outI] = self[(selfJ - 1) * self.width + selfI]
		end
	end
	for _, face in pairs(self.connections) do
    for _, c in ipairs(face) do
      output.connections:add(c.j, self.width - c.i - c.width + 2, c.height, c.width, (c.orientation + 1) % 4)
    end
	end
	for ie, e in ipairs(self.entrances) do
		output.entrances[ie] = {x = c.y, y = self.width - c.x + 1, r = c.r}
	end
	for ia, a in ipairs(self.altars) do
		output.altars[ia] = {x = c.y, y = self.width - c.x + 1, r = c.r}
	end
	return output
end

-- TODO: UNTESTED
-- Return a transposed copy of this rect (reflected around the diagonal).
function rect:transpose()
	local output = rect.new(self.height, self.width, tileTypes.IGNORE, self.x, self.y)
	for j = 1, self.height do
		for i = 1, self.width do
			output[(i - 1) * output.width + j] = self[(j - 1) * self.width + i]
		end
	end
	for ic, c in ipairs(self.connections) do
		-- It's a bit tricky to change orientations, so we've done it case-by-case.
		local newOrientation = orientation.SOUTH -- Corresponds to original = EAST
		if c.orientation == orientation.NORTH then
			newOrientation = orientation.WEST
		elseif c.orientation == orientation.WEST then
			newOrientation = orientation.NORTH
		elseif c.orientation == orientation.SOUTH then
			newOrientation = orientation.EAST
		end
		-- output.connections[ic] = {i = c.j, j = c.i, width = c.height, height = c.width, orientation = newOrientation}
		output.connections:add(c.j, c.i, c.height, c.width, newOrientation)
	end
	for ie, e in ipairs(self.entrances) do
		output.entrances[ie] = {x = c.y, y = c.x, r = c.r}
	end
	for ia, a in ipairs(self.altars) do
		output.altars[ia] = {x = c.y, y = c.x, r = c.r}
	end
	return output
end

-- TODO: ALLOW FOR CHOOSING INDEX OR AT LEAST DIRECTION?
-- Connect first available connection in "orientation" with first available from "other".
-- Returns success.
function rect:connectTo(orientation, other)

	local thisConnection = self.connections:getFirstFree(orientation)
	local otherConnection = other.connections:getFirstFree((orientation + 2) % 4)

	-- Check that we're connecting N-S or E-W.
	if (not thisConnection) or (not otherConnection) then
		return false
	end

  thisConnection.linkedTo = otherConnection
  otherConnection.linkedTo = thisConnection

	local ox = other.x + otherConnection.i
	local oy = other.y + otherConnection.j
	local sx = self.x  + thisConnection.i
	local sy = self.y  + thisConnection.j

	local x, y = ox - sx, oy - sy

	if thisConnection.orientation == basic.orientation.EAST then
		x = x - thisConnection.width
	elseif thisConnection.orientation == basic.orientation.NORTH then
		y = y + otherConnection.height
	elseif thisConnection.orientation == basic.orientation.WEST then
		x = x + otherConnection.width
	else -- SOUTH
		y = y - thisConnection.height
	end

	-- TODO: Check sizes match!
	self.x, self.y = x, y
	return true
end

-- TODO: UNTESTED!
-- Move this rect by an increment (dX,dY) and drag all connected rects by the same amount.
function rect:dragConnected(dX, dY)
  local visited = {self}
  local pending = {self}
  while #pending > 0 do
    local next = pending[#pending]
    pending[#pending] = nil
    next.x = next.x + dX
    next.y = next.y + dY
    for _, face in pairs(next.connections) do
      for _, connection in ipairs(face) do
        if connection.linkedTo then
          local newRect = connection.linkedTo
          local found = false
          for _, r in ipairs(visited) do
            if r == newRect then
              found = true
              break
            end
          end
          if not found then
            pending[#pending + 1] = newRect
            visited[#visited + 1] = newRect
          end
        end
      end
    end
  end
end

-- TODO UNTESTED!
-- Check if this rect overlaps "other" rect.
function rect:overlaps(other)
  local flagX, flagY = false, false

  if self.x <= other.x then
    if other.x <= self.x + self.width then
      flagX = true
    end
  else
    if self.x <= other.x + other.width then
      flagX = true
    end
  end

  if self.y <= other.y then
    if other.y <= self.y + self.height then
      flagY = true
    end
  else
    if self.y <= other.y + other.height then
      flagY = true
    end
  end

  return flagX and flagY
end

-- TODO: This is used by graph. Move it there? Split rect class into class family? Make separate classes?
-- Move this rect so it lays touching "other" on one of the sides. Center the free coordinate wrt "other"'s center.
function rect:setAdjacentCenteredTo(orientation, other)
  local x, y = other.x, other.y
  if orientation == basic.orientation.EAST then
    x = x - self.width
    y = y + math.floor((other.height - self.height) / 2)
  elseif orientation == basic.orientation.NORTH then
    x = x + math.floor((other.width - self.width) / 2)
    y = y - self.height
  elseif orientation == basic.orientation.WEST then
    x = x + other.width
    y = y + math.floor((other.height - self.height) / 2)
  elseif orientation == basic.orientation.SOUTH then
    x = x + math.floor((other.width - self.width) / 2)
    y = y + other.height
  end -- DEFAULT CASE: WRONG VALUE FOR ORIENTATION. FOR NOW DO NOTHING.
  self.x, self.y = x, y
end

-- Get a list of relatively positioned rects and move them so they'd fit into a padded rect with top coordinates 0,0.
-- Doesn't actually create the new rect.
-- Offsets the positions of the rects in the list.
-- Returns the dimensions the enclosing rect would have.
function rect.position(list, padding)
	if not list or #list < 1 then return nil end
	padding = padding or 1

	local minX, minY = list[1].x, list[1].y
	local maxX, maxY = minX + list[1].width, minY + list[1].height
	for i = 2, #list do
		local x1, y1 = list[i].x, list[i].y
		local x2, y2 = x1 + list[i].width, y1 + list[i].height
		minX = minX < x1 and minX or x1
		minY = minY < y1 and minY or y1
		maxX = maxX > x2 and maxX or x2
		maxY = maxY > y2 and maxY or y2
	end

	local width, height = maxX - minX + 2 * padding, maxY - minY + 2 * padding
	local offsetX, offsetY = padding - minX + 1, padding - minY + 1

	for _, rect in ipairs(list) do
		rect.x, rect.y = rect.x + offsetX, rect.y + offsetY
	end

  return width, height
end

-- Get a list of relatively positioned rects and coalesce them into a single big padded rect.
-- Returns the coalesced rect.
function rect.coalesce(list, padding)
	local width, height = rect.position(list, padding)
	local output = rect.new(width, height, IGNORE)
	for _, rect in ipairs(list) do
		output:copy(rect)
	end
	return output
end


-- DEBUG STUFF AHEAD
function rect:print()
	for j = 1, self.height do
		for i = 1, self.width do
			local letter = '?'
			local tile = self:getTile(i,j)
			if     tile == WALL     then letter = 'W'
			elseif tile == FLOOR    then letter = 'F'
			elseif tile == ENTRANCE then letter = 'E' end
			io.write(letter, " ")
		end
		io.write("\n")
	end
end

return rect
