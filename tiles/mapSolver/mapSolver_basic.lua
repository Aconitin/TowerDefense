local basic = {}

basic.tileTypes = {
  NONE = 0,
  FIRST = 1,

  WALL = 1,
  FLOOR = 2,
  ENTRANCE = 3,
  ALTAR = 4,
  WATER = 5,

  LAST = 5,
  IGNORE = 6,
  DEFAULT = 1 -- WALL
}

-- Final types are the ones that can be converted to tileMap entries.
function basic.tileTypes.isFinal(type)
  return type >= basic.tileTypes.FIRST and type <= basic.tileTypes.LAST
end

basic.orientation = {
  EAST = 0,
  NORTH = 1,
  WEST = 2,
  SOUTH = 3
} -- How many times do we rotate?

return basic
