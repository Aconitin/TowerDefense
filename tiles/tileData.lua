local tileData = {}

function tileData.new()
	local output = {}
	setmetatable(output, {__index = tileData})
	return output
end

function tileData:add(passable, rgb)
	local arch = {
		id = #self + 1,
		passable = passable,
		color = {r = rgb.r, g = rgb.g, b = rgb.b}
	}
	self[#self + 1] = arch
	return #self
end

return tileData
