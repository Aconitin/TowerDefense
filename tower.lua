local tower = {}

tower.towers = {} --> List of instance references!
tower.tileLookup = {} --> Short dict-like lookup table for tile obstructedness!

--> Creates a new tower instance!
function tower:new(i, j)

--[[ Parameters:
	
	Returns true or false depending on wether placement was successfull!

]]--

	--> Check if all tiles that this tower will be placed on are free!
	if not self:tilesFree({{i = i, j = j}}) then
		return false
	end
	
	--> Create a new tower entity!
	local t = entities["tower"](i + 0.5, j + 0.5)
	
	
	
	
	engine:addEntity(t)

end

--> Deletes a tower instance!
function tower:delete()





end

--> Registers or unregisters one tile in the lookup table!
function tower:setTile(i, j, refOrFalse)

	if not self.tileLookup[i] then
		self.tileLookup[i] = {}
	end

	self.tileLookup[i][j] = refOrFalse

end

--> Returns a boolean indicating wether any of the given tiles are obstructed _by a tower_!
function tower:tilesFree(tiles)

	for _,t in pairs(tiles) do
		if self.tileLookup[t.i] and self.tileLookup[t.i][t.j] then
			return false
		end
	end
	
	return true

end

return tower