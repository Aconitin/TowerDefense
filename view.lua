--TODO: fix zooming, create debug option/print, moveTo() API, zoomTo() API, maybe use hump timer or some smoothing shit, integrate setNewViewSize()

local camera = require("lib.camera") --> Camera library for ingame viewports!

local view = {}

--> Create new view!
function view.new(parameters)
	
	--> Create object!
	local v = {}
	
	--> Updates a given view!
	function v:update(dt)
	
		--> Update camera zoom smoothing time!
		self.cameraZoomT = math.clamp(0, self.cameraZoomT + 2*dt, 1)
		self.cameraZoom = math.lerp(self.cameraZoom, self.cameraZoomTo, self.cameraZoomT)
		self.cameraZoom = math.clamp(self.cameraMinZoom, self.cameraZoom, self.cameraMaxZoom)
		self.camera:zoomTo(self.cameraZoom)

		--> Update camera lerp to position!
		self.cameraLerpT = math.clamp(0, self.cameraLerpT + 10*dt, 1)
		self.camera.x = math.clamp(0, math.lerp(self.camera.x, self.cameraLerpTo.x, self.cameraLerpT), self.viewSize.w)
		self.camera.y = math.clamp(0, math.lerp(self.camera.y, self.cameraLerpTo.y, self.cameraLerpT), self.viewSize.h)
	
	end
	
	--> Debug switch!
	function v:debug() self.debug = not self.debug end
		
	--> Sets parameters/instance variables!
	function v:set(p, hardReset)
	
		if hardReset then
			self.debug = nil
			self.sensitivity = nil
			self.viewSize = nil
			self.cameraBorder = nil
			self.cameraLerpT = nil
			self.cameraZoomT = nil
			self.cameraMinZoom = nil
			self.cameraMaxZoom = nil
			self.cameraZoom = nil
			self.cameraZoomTo = nil
			self.cameraLerpTo = nil
		end
	
		local p = p or {}
		self.debug = p.debug or self.debug or true
		self.sensitivity = p.sensitivity or self.sensitivity or 400
		self.viewSize = p.viewSize or self.viewSize or {w = 0.5 * love.graphics.getWidth(), h = 0.5 * love.graphics.getHeight()}
		self.cameraBorder = p.cameraBorder or self.cameraBorder or 0
		self.cameraLerpT = p.cameraLerpT or self.cameraLerpT or 1
		self.cameraZoomT = p.cameraZoomT or self.cameraZoomT or 1
		self.cameraMinZoom = p.cameraMinZoom or self.cameraMinZoom or math.min(love.graphics.getWidth()/(self.viewSize.w + self.cameraBorder * 2), love.graphics.getHeight()/(self.viewSize.h + self.cameraBorder * 2))
		self.cameraMaxZoom = p.cameraMaxZoom or self.cameraMaxZoom or 1
		self.cameraZoom = p.cameraZoom or self.cameraZoom or self.cameraMinZoom
		self.cameraZoomTo = p.cameraZoomTo or self.cameraZoomTo or self.cameraZoom
		self.cameraLerpTo = p.cameraLerpTo or self.cameraLerpTo or {x = 0.5 * self.viewSize.w, y = 0.5 * self.viewSize.h}
	end
	
	--> Wrappers for enabling and disabling!
	function v:enable() return self.camera:attach() end
	function v:disable() return self.camera:detach() end
	
	--> Move the camera around by some specific values (keyboard movement)!
	function v:kbMovement(dx, dy)
		self.cameraLerpT = 0
		self.cameraLerpTo.x = math.clamp(0, self.cameraLerpTo.x + self.sensitivity * dx, self.viewSize.w)
		self.cameraLerpTo.y = math.clamp(0, self.cameraLerpTo.y + self.sensitivity * dy, self.viewSize.h)
	end

	--> Move the camera around by some specific values (mouse movement)!
	function v:mouseMovement(dx, dy)
		self.cameraLerpT = 0
		self.cameraLerpTo.x = math.clamp(0, self.cameraLerpTo.x + dx, self.viewSize.w)
		self.cameraLerpTo.y = math.clamp(0, self.cameraLerpTo.y + dy, self.viewSize.h)
	end
	
	--> Zoom the camera by some value!
	function v:wheelMovement(dx, dy)
		self.cameraZoomTo = math.clamp(self.cameraMinZoom, self.cameraZoomTo + 0.1 * dy, self.cameraMaxZoom)
		self.cameraZoomT = 0
	end
	
	--> Add some instance variables!
	v:set(parameters)
	
	--> Initialize camera and move to correct initial position!
	v.camera = camera.new()
	v.camera:zoom(v.cameraMinZoom) --> Set camera initial zoom!
	
	--> Back to you, Jimmy!
	return v

end

return view