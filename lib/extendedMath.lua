--> This heavily extends the math library!

-- Returns the distance between two points.
function math.dist(x1,y1, x2,y2) return ((x2-x1)^2+(y2-y1)^2)^0.5 end

-- Returns the squared distance between two points (faster).
function math.distSqr(x1,y1, x2,y2) return (x2 - x1)^2 + (y2 - y1)^2 end -- TODO: Does it do square efficiently?

-- Returns the angle between two points.
function math.angle(x1,y1, x2,y2) return math.atan2(y2-y1, x2-x1) end

--Returns the midpoint between two points
function math.midpoint(x1,y1,x2,y2) return 0.5*(x1+x2),0.5*(y1+y2) end

-- Clamps a number to within a certain range.
function math.clamp(low, n, high) return math.min(math.max(low, n), high) end

-- Cosine interpolation between two numbers.
function math.cerp(a,b,t) local f=(1-math.cos(t*math.pi))*.5 return a*(1-f)+b*f end

-- Normalize two numbers.
function math.normalize(x,y) local l=(x*x+y*y)^.5 if l==0 then return 0,0,0 else return x/l,y/l,l end end

-- Returns 'n' rounded to the nearest 'deci'th (defaulting whole numbers).
function math.round(n, deci) deci = 10^(deci or 0) return math.floor(n*deci+.5)/deci end

-- Randomly returns either -1 or 1.
function math.rsign() return love.math.random(2) == 2 and 1 or -1 end

-- Returns 1 if number is positive, -1 if it's negative, or 0 if it's 0.
function math.sign(n) return n>0 and 1 or n<0 and -1 or 0 end

-- Gives a precise random decimal number given a minimum and maximum
function math.prandom(min, max) return love.math.random() * (max - min) + min end

--> Gives the sum of all values in a table!
function math.sumOfTable(t)
    local v = 0
    for i=1,#t,1 do
        v = v + t[i]
    end
    return v
end

function math.map(x, minFrom, maxFrom, minTo, maxTo)
	return minTo + ((maxTo - minTo) * (x - minFrom)) / (maxFrom - minFrom)
end

function math.mapFromUnit(x, minTo, maxTo)
	return minTo + (maxTo - minTo) * x
end

function math.mapToUnit(x, minFrom, maxFrom)
	return (x - minFrom) / (maxFrom - minFrom)
end

-- TODO: UNCHECKED
-- Linearly interpolate between a (t <= 0) and b (t >=1) using the interpolation
-- parameter t (clamped, uses sum).
function math.lerp(a, b, t)
	if t <= 0 then return a elseif t >= 1 then return b end
	return (1 - t) * a + t * b
end

-- TODO: UNCHECKED
-- Linearly interpolate between a (t = 0) and b (t =1) using the interpolation
-- parameter t (built for speed, not clamped).
function math.lerpUnclamped(a, b, t)
	return a + (b - a) * t
end

-- TODO: UNCHECKED
-- C^1 interpolation between 0 and 1
function math.smoothStep(t)
	if t <= 0 then return 0 elseif t >= 1 then return 1 end
	return t * t * (3 - 2 * t)
end

-- TODO: UNCHECKED
-- C^2 interpolation between 0 and 1
function math.smootherStep(t)
	if t <= 0 then return 0 elseif t >= 1 then return 1 end
	return t * t * t * (10 + t * (-15 + 6 * t))
end

-- TODO: UNCHECKED
-- interpolate between a and b with interpolation parameter t by calling f(t)
-- (assumes f(0) = 0, f(1) = 1).
function math.interpolate(a, b, t, f)
	if t <= 0 then return a elseif t >= 1 then return b end
	local p = f(t)
	return (1 - p) * a + p * b
end
