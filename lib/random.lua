-- Random number generator library that expands the facilities offered by LÖVE.
-- TODO: Functions with documentation are tested.

local random = {}

function random.new(seed)
	local generator = seed and love.math.newRandomGenerator(seed) or love.math.newRandomGenerator()
	local output = setmetatable({_generator = generator}, {__index = random})
	return output
end

function random:getSeed()
	return self._generator:getSeed()
end

function random:getState()
	return self._generator:getState()
end

function random:setSeed(seed)
	self._generator:setSeed(seed)
end

function random:setState(state)
	return self._generator:setState(state)
end

-- Get a uniformly distributed random number in a continuous interval.
--   () -> interval (0, 1).
--   (k) -> interval (0, k)
--   (min, max) -> interval (min, max)
function random:uniform(a, b)
	if not a then
		return self._generator:random()
	elseif not b then
		return a * self._generator:random()
	else
		return a + (b - a) * self._generator:random()
	end
end

-- Get a uniformly distributed random integer.
--   (k) -> {0, ..., k-1}
--   (min, max) -> {min, ..., max}
function random:uniformInt(a, b)
	if not b then
		return math.floor(a * self._generator:random())
	else
		return a + math.floor((b - a + 1) * self._generator:random())
	end
end

function random:normal(mean, deviation)
	if not mean then
		return self._generator:randomNormal()
	elseif not deviation then
		return mean + self._generator:randomNormal()
	else
		return mean + deviation * self._generator:randomNormal()
	end
end

-- Return true with probability p and false otherwise.
function random:bernoulli(p)
	return self._generator:random() < p
end

function random:dice(number, faces)
	output = 0
	for i = 1, number do
		output = output + self.uniformInt(1, faces)
	end
end

function random:irwinHall(n, min, max)
	output = 0
	for i = 1, n do
		output = output + self._generator:random()
	end
	return mathUtils.map(output, 0, n, min, max)
end

function random:boundedBell(min, max)
	return self:irwinHall(4, min, max)
end

-- TODO: UNCHECKED
-- Returns a random entry from the given array, preserving the array.
function random:array(t)
	return t[self:uniformInt(1, #t)]
end

-- TODO: UNCHECKED
-- Returns a random entry from the given array and removes it from the array.
function random:arrayPop(t)
	return t.remove(self:uniformInt(1, #t))
end

-- TODO: UNCHECKED
-- Returns a random key from the given table, using the values as weights. Preserves
-- the table.
function random:weighted(weightTable)
	if not weightTable then return nil end
	local totalWeight = 0
	for entry, weight in pairs(weightTable) do
		totalWeight = totalWeight + weight
	end
	local criticalWeight = self:uniform(totalWeight)
	local cumulatedWeight = 0
	for entry, weight in pairs(weightTable) do
		cumulatedWeight = cumulatedWeight + weight
		if cumulatedWeight > criticalWeight then
			return entry
		end
	end
	return nil -- SHOULD NEVER HAPPEN!
end

-- TODO: UNCHECKED
-- Returns a random key from the given table, using the values as weights. Removes
-- the returned element from the table.
function random:weightedPop(weightTable)
	local key = self:weighted(weightTable)
	weightTable.remove(key)
	return key
end

-- TODO: UNCHECKED
-- Shuffles array in-place (Fisher-Yates algorithm).
function random:shuffle(t)
	for i = #t, 1, -1 do
		local j = self:uniformInt(1, i)
		t[i], t[j] = t[j], t[i]
	end
end

return random
