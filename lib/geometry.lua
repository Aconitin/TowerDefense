local geometry = {}

-- Enumeration with the geometry types implemented so far.
local TUPLE, VECTOR, POINT, LINE = 1, 2, 3, 4

-- Quick check that a and b are geometry objects from this package
local function areGeoTypes(a, b)
	if a and b and a.geoType and b.geoType then
		return true
	else
		return false
	end
end

-- This is used by all the tuple subclasses
function geometry.fromCoords(index, ...)
	local output = setmetatable({}, {__index = index})
	for i = 1, #args do
		output[i] = args[i]
	end
	return output
end

-- TUPLE: Base class for points and vectors.
-- tupleMetatable implements operator overriding for tuple sum and tuple scalar multiplication.
local tupleMetatable = {
	__add = function(lhs, rhs)
		if not areGeoTypes(lhs, rhs) then return nil end
		local geoType = (lhs.geoType == rhs.geoType and lhs.geoType) or
						(lhs.geoType == POINT and rhs.geoType == VECTOR and POINT) or
						(lhs.geoType == VECTOR and rhs.geoType == POINT and POINT) or
						TUPLE
		local index = (geoType == POINT and geometry.point) or
					  (geoType == VECTOR and geometry.vector) or
					  geometry.tuple
		local output = setmetatable({}, {__index = index})
		return lhs:clone():add(rhs)
	end,
	__mul = function(lhs, rhs)
		if type(lhs) == "number" and rhs.geoType then
			return rhs:clone():scale(lhs)
		else if lhs.geoType and type(rhs) == "number" then
			return lhs:clone():scale(rhs)
		else if areGeoTypes(lhs, rhs) and lhs.geoType == VECTOR and rhs.geoType == VECTOR then
			return lhs:dot(rhs)
		end
		return nil
	end
}

geometry.tuple = setmetatable({ geoType = TUPLE }, tupleMetatable)

function geometry.tuple.fromCoords(index, ...)
	return geometry.fromCoords(geometry.tuple, ...)
end

function geometry.tuple:clone()
	local output = setmetatable({}, {__index = geometry.tuple})
	for i = 1, #self do
		output[i] = self[i]
	end
	return output
end

function geometry.tuple:x() return self[1] end
function geometry.tuple:y() return self[2] end
function geometry.tuple:z() return self[3] end
function geometry.tuple:w() return self[4] end

function geometry.tuple:r() return self[1] end
function geometry.tuple:g() return self[2] end
function geometry.tuple:b() return self[3] end
function geometry.tuple:a() return self[4] end

-- Destructive scalar multiplication
function geometry.tuple:scale(scalar)
	for i = 1, #self do
		self[i] = scalar * self[i]
	end
	return self
end

-- Destructive tuple addition
function geometry.tuple:add(other)
	for i = 1, #other do
		self[i] = self[i] + other[i]
	end
	return self
end

geometry.vector = setmetatable({ geoType = VECTOR }, {__index = geometry.tuple})

function geometry.tuple.fromCoords(index, ...)
	return geometry.fromCoords(geometry.vector, ...)
end

function geometry.vector.fromPoints(origin, destination)
	local output = setmetatable({}, {__index = geometry.vector})
	for i = 1, #args do
		output[i] = destination[i] - origin[i]
	end
	return output
end

function geometry.vector:dot(other)
	output = 0
	for i = 1, (#self > #other and #other or #self) do
		output = output + self[i] * other[i]
	end
	return output
end

function geometry.vector:magnitudeSquared()
	local output = 0
	for i = 1, #self do
		output += self[i]^2
	end
	return output
end

function geometry.vector:magnitude()
	return self:magnitudeSquared()^0.5
end

function geometry.vector:normalize()
	return self:scale(self:magnitude())
end

function geometry.vector:setMagnitude(magnitude)
	return self:normalize():scale(maxLength)
end

function geometry.vector:trim(maxLength)
	if self:magnitudeSquared() > maxLength^2 then
		return self:setMagnitude(maxLength)
	end
end

function geometry.vector:projectInto(other)
	return other:clone():scale(other:dot(self))
end

geometry.point = setmetatable({ geoType = POINT }, {__index = geometry.tuple})

function geometry.point.fromCoords(index, ...)
	return geometry.fromCoords(geometry.point, ...)
end

geometry.line = { geoType = LINE }

function geometry.line.fromPointVector(origin, direction)
	return setmetatable({origin = origin, direction = direction}, {__index = geometry.line})
end

function geometry.line.fromPoints(p1, p2)
	local v = geometry.vector.fromPoints(p1, p2)
	return geometry.line.fromPointVector(p1, v)
end

function geometry.distancePointToPoint(a, b)
	local output = 0
	for i = 1, #a do
		output = output + (b[i] - a[i])^2
	end
	return output^0.5
end

function geometry.distancePointToLine(p, l)
	local v = vector.fromPoints(p, l.origin)
	return v:projectInto(l.direction):magnitude()
end

function geometry.d(a, b)
	if not areGeoTypes(a, b) then return nil end
	if a.geoType > b.geoType then -- Only check in ascending order: POINT, LINE
		a, b = b, a
	end
	if a.geoType == POINT then
		if b.geoType == POINT then
			return geometry.distancePointToPoint(a, b)
		elseif b.geoType == LINE then
			return geometry.distancePointToLine(a, b)
		end
	elseif a.geoType == LINE then -- Only LINE to LINE left
		return geometry.distanceLineToLine(a, b)
	end
	return nil
end

return geometry
