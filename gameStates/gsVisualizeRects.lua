local state = {}

local TILE_SIZE = 8
local width, height

local rng
local list
local posX, posY, offX, offY, w, h = {}, {}, {}, {}, {}, {}
local index = 1

function state.onStart()
  love.window.setTitle("Tower Defense (rect visualizing debug tool)")
  width, height = love.graphics.getDimensions()
  love.graphics.setBackgroundColor(50, 70, 60)
  -- solver = mapSolver.new(tmBasic.tileData, tmBasic.mapping) --> Instance of "mapSolver" that takes care of creating the solver.tileMap and populating it.
	-- solver:solve()
  rng = random.new(love.timer.getTime())
  list = mapSolver.graph.solve({}, rng)
  for i, rectList in ipairs(list) do
    -- COPIED FROM rect.position TO AVOID A BUG INVOLVING REPETITION -----------
    local minX, minY = rectList[1].x, rectList[1].y
    local maxX, maxY = minX + rectList[1].width, minY + rectList[1].height
    for i = 2, #rectList do
      local x1, y1 = rectList[i].x, rectList[i].y
      local x2, y2 = x1 + rectList[i].width, y1 + rectList[i].height
      minX = minX < x1 and minX or x1
      minY = minY < y1 and minY or y1
      maxX = maxX > x2 and maxX or x2
      maxY = maxY > y2 and maxY or y2
    end
    w[i], h[i] = maxX - minX, maxY - minY
    posX[i], posY[i] = 1 - minX, 1 - minY
    ----------------------------------------------------------------------------

    -- Above offsets would put map at upper left. Let's store centered values:
    local WW, HH = width / TILE_SIZE, height / TILE_SIZE
    offX[i], offY[i] = math.floor((WW - w[i]) / 2), math.floor((HH - h[i]) / 2)
  end

  -- DEBUG
  -- print("[gsVisualizeRects @ onStart] ================ START PRINT RECTS ================")
  -- for i, rectList in ipairs(list) do
  --   print("  -------- Index ", i, " --------")
  --   for j, rect in ipairs(rectList) do
  --     print("    " .. (rect.special and "[" or "(") .. "rect #" .. j .. (rect.special and "]" or ")"), "x = " .. rect.x .. ",", " y = " .. rect.y .. ",", " width = " .. rect.width .. ",", " height = " .. rect.height, (rect.numEntrances and "ENTRANCE" or (rect.numAltars and "ALTAR" or "normal")))
  --   end
  -- end
  -- print("[gsVisualizeRects @ onStart] ================= END PRINT RECTS =================")
  -- !DEBUG
end

function state.update(dt)
  -- TODO
end

local function getRectColor(rect)
  if not rect then
    return 0, 0, 0
  elseif not rect.nonterminal then
    return 80, 80, 80
  elseif rect.numAltars then
    return 80, 150, 120
  elseif rect.numEntrances then
    return 140, 100, 110
  else
    return 110, 110, 120
  end
end

function state.draw()
  -- Draw tile grid.
  love.graphics.setLineWidth(1)
  love.graphics.setColor(20, 200, 240, 255)
  for y = 0, height, TILE_SIZE do
    love.graphics.line(0, y, width, y)
  end
  for x = 0, width, TILE_SIZE do
    love.graphics.line(x, 0, x, height)
  end

  -- Draw bounding box.
  love.graphics.setLineWidth(2)
  love.graphics.setColor(255, 255, 100, 255)
  love.graphics.rectangle("line", (offX[index] + 1) * TILE_SIZE, (offY[index] + 1) * TILE_SIZE, w[index] * TILE_SIZE, h[index] * TILE_SIZE)

  -- Draw rects.
  for _, rect in ipairs(list[index]) do
    local r, g, b = getRectColor(rect)
    love.graphics.setColor(r, g, b, 150)
    love.graphics.rectangle("fill", (offX[index] + posX[index] + rect.x) * TILE_SIZE, (offY[index] + posY[index] + rect.y) * TILE_SIZE, rect.width * TILE_SIZE, rect.height * TILE_SIZE)
    love.graphics.setColor(r, g, b, 255)
    love.graphics.rectangle("line", (offX[index] + posX[index] + rect.x) * TILE_SIZE, (offY[index] + posY[index] + rect.y) * TILE_SIZE, rect.width * TILE_SIZE, rect.height * TILE_SIZE)
  end

  -- Draw text box
  love.graphics.setColor(0, 0, 0, 200)
  love.graphics.rectangle("fill", 0, 0, 200, 200)
  love.graphics.setColor(200, 200, 200, 255)
  love.graphics.print("FPS: " .. love.timer.getFPS() .. "\n" .. index .. "/" .. #list, 10, 10)
end

function state:keyReleased(key, scancode)
  if key == "right" or key == "d" then
    index = index + 1
  elseif key == "left" or key == "a" then
    index = index - 1
  elseif key == "1" then
    index = 1
  elseif key == "2" then
    index = 2
  elseif key == "3" then
    index = 3
  elseif key == "4" then
    index = 4
  elseif key == "5" then
    index = 5
  elseif key == "6" then
    index = 6
  elseif key == "7" then
    index = 7
  elseif key == "8" then
    index = 8
  elseif key == "9" then
    index = 9
  elseif key == "0" then
    index = 10
  end
  index = math.clamp(1, index, #list)
end

return state
