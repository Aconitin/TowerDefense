local gsMainGame = {} --> TBR!

--> Gets triggered once on gamestate switch!
function gsMainGame:onStart()

	--> Create and initialize a solver and tileMap!
	solver = mapSolver.new(tmBasic.tileData, tmBasic.mapping) --> Instance of "mapSolver" that takes care of creating the solver.tileMap and populating it.
	solver:solve()

	--> Create a view/camera
	local field = solver.tileMap.playfield
	camera = view.new{
			viewSize = {w = field.width * globals.unitScale.x, h = solver.tileMap.height * globals.unitScale.y},
			cameraLerpTo = {x = (field.x - 1 + 0.5 * field.width) * globals.unitScale.x, y = (field.y - 1 + 0.5 * field.height) * globals.unitScale.y}
		} --> Instance of "view" responsible for coordinate system transformations for rendering entities!
		
	--> Create a roundKeeper that is responsible for waves!
	dungeonMaster = roundKeeper()

engine:addEntity(entities["cursor"]())

end

--> Update callback!
function gsMainGame:update(dt)

	timer.update(dt) --> Update timer library!
	dungeonMaster:update(engine, solver.tileMap, dt) --> Update waves!
	camera:update(dt) --> Update camera interpolation/smoothing!
	engine:update(dt)

end

--> Render callback!
function gsMainGame:draw()

	camera:enable() --> Start looking through the camera!

		solver.tileMap:render() --> Render the map!
		engine:draw()
		debugging:drawWithCamera(engine, solver) --> Draw camera-bound debugging stuff (like the grid)!

	camera:disable() --> Stop looking through the camera!

	debugging:drawOverlay(engine, solver, dungeonMaster) --> Draw debug info!

end

--> Callback for mousepresses!
function gsMainGame:mouseReleased(x, y, button, istouch)
	x, y = camera.camera:worldCoords(x, y)
	local i, j = solver.tileMap:convert("pcti", x, y)
	tower:new(i, j)
end

--> Callback for keyreleases!
function gsMainGame:keyReleased(key, scancode)
	debugging:trigger(key) --> Debugging hook!
end

--> Callback for movement!
function gsMainGame:moved(dx, dy)
	camera:kbMovement(dx, dy)
end

--> Callback for scrolling!
function gsMainGame:wheelMoved(dx, dy)
	camera:wheelMovement(dx, dy)
end

--> Calling for mouseMovement!
function gsMainGame:mouseMoved(x, y, dx, dy, istouch)
	if love.mouse.isDown("2") then
		camera:mouseMovement(dx, dy)
	end
end

--> Back to you, Jimmy!
return gsMainGame
