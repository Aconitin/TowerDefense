-- This game state shows a logo and TODO: loads assets in the background (see library
-- https://github.com/kikito/love-loader).

local gsSplashScreen = {}

local imagePath = "res/logo-back.jpg"
local duration = 0 -- In seconds
-- local duration = 1 -- In seconds

local image
local elapsed
local oldWidth, oldHeight

function gsSplashScreen.onStart()
	image = love.graphics.newImage(imagePath)
	oldWidth, oldHeight = love.graphics.getDimensions()
	love.window.setMode(image:getDimensions())
	elapsed = 0
end

function gsSplashScreen.onExit()
	love.window.setMode(oldWidth, oldHeight)
end

function gsSplashScreen.update(dt)
	elapsed = elapsed + dt
	if elapsed > duration then
		gameState.setActive(gsMainGame)
	end
end

function gsSplashScreen.draw()
	love.graphics.draw(image, 0, 0)
	-- love.graphics.print(tostring(elapsed) .. " / " .. tostring(duration), 10, 10)
end

return gsSplashScreen
