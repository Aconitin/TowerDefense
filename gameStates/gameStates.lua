local gameStates = {}

--[[ A gameStates may implement any of the following methods:

draw()
update(dt)

onExit()
onStart()

keypressed(key, scancode, isRepeat)
keyreleased(key, scancode)
mousemoved(x, y, dx, dy, istouch)
mousepressed(x, y, button, istouch)
mousereleased(x, y, button, istouch)
resize(w, h)
textinput(text)
wheelmoved(dx, dy)
moved(dx, dy)

IMPORTANT: Note that all calls are done via active:func, ergo every first argument must be 'self'!

--]]

gameStates.active = {}
gameStates["splashScreen"] = require("gameStates.gsSplashScreen")
gameStates["mainGame"] = require("gameStates.gsMainGame")
gameStates["visualizeRects"] = require("gameStates.gsVisualizeRects")

function gameStates:setActive(name)
    local active = gameStates.active
	local new = self[name]
    if active and active.onExit then active:onExit() end
    if new and new.onStart then new:onStart() end
    gameStates.active = new
end

return gameStates
