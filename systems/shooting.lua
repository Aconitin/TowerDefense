-- 88888888ba                                     88888888ba                                     88888888ba                                  
-- 88      "8b                                    88      "8b                                    88      "8b                                 
-- 88      ,8P                                    88      ,8P                                    88      ,8P                                 
-- 88aaaaaa8P'  ,adPPYba,  8b      db      d8     88aaaaaa8P'  ,adPPYba,  8b      db      d8     88aaaaaa8P'  ,adPPYba,  8b      db      d8  
-- 88""""""'   a8P_____88  `8b    d88b    d8'     88""""""'   a8P_____88  `8b    d88b    d8'     88""""""'   a8P_____88  `8b    d88b    d8'  
-- 88          8PP"""""""   `8b  d8'`8b  d8'      88          8PP"""""""   `8b  d8'`8b  d8'      88          8PP"""""""   `8b  d8'`8b  d8'   
-- 88          "8b,   ,aa    `8bd8'  `8bd8'       88          "8b,   ,aa    `8bd8'  `8bd8'       88          "8b,   ,aa    `8bd8'  `8bd8'    
-- 88           `"Ybbd8"'      YP      YP         88           `"Ybbd8"'      YP      YP         88           `"Ybbd8"'      YP      YP      


--> This system deals with aiming and shooting of towers at enemies!
local shooting = hooECS.System.create("shooting")
local aiming = {} --> Little 'subclass' that helps with aiming!
local debugDraw = true

--> Init class!
function shooting:initialize()
	hooECS.System.initialize(self)
end

--> Main update call!
function shooting:update(dt)
	for _,tower in pairs(self.targets) do

		--> Count down tower shot countdown!
		tower:get("countdown"):count(dt)
		
		--> Check if shot is ready!
		if tower:get("countdown").ready then
			
			--> Make sure tower has a valid target!			
			local ct = aiming:hasValidTarget(tower)
			if not ct then --> If target is not valid, make sure it's set to nil, then re-aim!
				tower:get("target"):set(nil)
				tower:get("target"):set(aiming:aim(tower)) --> Aim at something!
			end

			--> If the tower successfully aquired a new target that is in range, it can finally shoot!
			if aiming:hasValidTarget(tower) then
				self:shoot(tower)
				tower:get("countdown"):reset()
				
				--> If tower is supposed to drop target after each shot, do that!
				if tower:get("isTower").retargetAfterShot then
					tower:get("target"):set(nil)
				end
				
			end
			
		end
	end
end

--> Debug draw system!
function shooting:draw()

	if debugDraw then

		local unitScale = _G.globals.unitScale

		for _,tower in pairs(self.targets) do

			--> Draw range circle!
			shooting:drawRangeCircle(tower)

			--> If tower has target, draw line to it!
			local target = tower:get("target").target
			if target then
				love.graphics.setColor(255, 100, 100, 255)
				local tox, toy = tower:get("position").x * unitScale.x, tower:get("position").y * unitScale.y
				local tax, tay = target:get("position").x * unitScale.x, target:get("position").y * unitScale.y
				love.graphics.line(tox, toy, tax, tay)
			end
			
			--> Draw countdown circle!
			love.graphics.setColor(255, 255, 255, 255)
			if tower:get("countdown").ready then
				love.graphics.setColor(255, 255, 255, 255)
			end
			love.graphics.setLineWidth(2)
			love.graphics.circle("fill", tower:get("position").x * unitScale.x, tower:get("position").y * unitScale.y, tower:get("countdown"):getPerc() * 0.75 * unitScale.x, 50)
			love.graphics.circle("line", tower:get("position").x * unitScale.x, tower:get("position").y * unitScale.y, 0.75 * unitScale.x, 50)
			
		end
	end
end

--> Draw range circle!
function shooting:drawRangeCircle(tower, color, transparency, lineWidth)
	local color = color or {0, 200, 200, 255} --> Color and transparency of line circle!
	local transparency = transparency or 50 --> Transparency of filled circle!
	local lineWidth = lineWidth or 4
	local unitScale = _G.globals.unitScale
	love.graphics.setLineWidth(lineWidth)
	love.graphics.setColor(color)
	love.graphics.circle("line", tower:get("position").x * unitScale.x, tower:get("position").y * unitScale.y, tower:get("range").range * unitScale.x, 50)
	love.graphics.setColor({color[1], color[2], color[3], transparency})
	love.graphics.circle("fill", tower:get("position").x * unitScale.x, tower:get("position").y * unitScale.y, tower:get("range").range * unitScale.x, 50)
end

--> Pew pew!
function shooting:shoot(tower)

	--> Parameters!
	local x = tower:get("position").x
	local y = tower:get("position").y
	local target = tower:get("target").target

	--> Create bullet entity!
	engine:addEntity(entities["bullet"](x, y, target))

end

--> Set targets for this system!
function shooting:requires()
	return({"isTower", --> Target must be a tower. Only towers can shoot!
			"position", --> Tower needs a position!
			"range", --> Tower needs a range!
			"target", --> Tower needs a target to aim at and shoot at!
			"countdown" --> Tower needs a countdown for its shot!
	})
end

--> Double-checks target validity: In range, alive, not removeable. Returns true if the given tower has a valid target!
function aiming:hasValidTarget(tower)

	--> First check if tower actually is in possession of a target!
	local target = tower:get("target").target --> Current target!
	if not target then
		return false
	end
	
	--> Now check if the target is flagged as removeable already!
	if target:get("removeable").removeable then
		return false
	end
	
	--> Now check if target is alive, i.e. health greater then 0!
	if target:get("health"):isDead() then
		return false
	end
	
	--> Now check if target is in range!
	if not self:checkIfInRange(tower, target) then
		return false
	end
	
	--> If all these tests were OK, return the target reference!
	return target

end

--> Aquires target!
function aiming:aim(tower)
	local aimMode = tower:get("isTower").shootingMode
	local func = self:mapMode(aimMode)
	return func(self, tower)
end

--> Maps shooting mode integers to functions!
function aiming:mapMode(int)
	if int == 1 then
		return self.fetchClosestTarget --> Aquire target: closest distance to tower position!
	elseif int == 2 then
		return self.fetchFarthestTarget --> Aquire target: farthest distance to tower position!
	elseif int == 3 then
		return self.fetchWeakestTarget --> Aquire target: weakest!
	elseif int == 4 then
		return self.fetchStrongestTarget --> Aquire target: strongest!
	end
end

--> Aquire target: closest distance to tower position!
function aiming:fetchClosestTarget(tower)

	--> Get a list of possible targets and tower position!
	local tl = engine:getEntitiesWithComponent("isEnemy")
	local towerPos = tower:get("position"):fetch()
	
	--> Get closest enemy!
	local closest = false
	local dist = math.huge
	for _,target in pairs(tl) do
		local tPos = target:get("position"):fetch()
		local newDist = math.dist(tPos.x, tPos.y, towerPos.x, towerPos.y)
		if newDist < dist then
			dist = newDist
			closest = target
		end
	end

	--> Return new target if it is in range!
	if dist <= tower:get("range").range then
		return closest
	end
		
end

--> Aquire target: farthest distance to tower position!
function aiming:fetchFarthestTarget(tower)

	--> Get a list of possible targets and tower position!
	local tl = engine:getEntitiesWithComponent("isEnemy")
	local towerPos = tower:get("position"):fetch()
	
	--> Get farthest enemy!
	local farthest = false
	local dist = 0
	for _,target in pairs(tl) do
		local tPos = target:get("position"):fetch()
		local newDist = math.dist(tPos.x, tPos.y, towerPos.x, towerPos.y)
		if newDist >= dist and newDist <= tower:get("range").range then
			dist = newDist
			farthest = target
		end
	end
	return farthest
end

--> Aquire target: Target in range with lowest health first!
function aiming:fetchWeakestTarget(tower)
	local weakest = false
	local hp = math.huge
	for _,target in pairs(engine:getEntitiesWithComponent("isEnemy")) do
		local newHP = target:get("health"):getComparable()
		if newHP < hp and self:checkIfInRange(tower, target) then
			hp = newHP
			weakest = target
		end
	end
	return weakest
end

--> Aquire target: Target in range with highest health first!
function aiming:fetchStrongestTarget(tower)
	local strongest = false
	local hp = 0
	for _,target in pairs(engine:getEntitiesWithComponent("isEnemy")) do
		local newHP = target:get("health"):getComparable()
		if newHP > hp and self:checkIfInRange(tower, target) then
			hp = newHP
			strongest = target
		end
	end
	return strongest
end

--> Returns bool indicating wether enemy e is in range of tower t!
function aiming:checkIfInRange(t, e)
	local ePos = e:get("position"):fetch()
	local tPos = t:get("position"):fetch()
	if math.dist(tPos.x, tPos.y, ePos.x, ePos.y) > t:get("range").range then
		return false
	end
	return true
end

--> Back to you, Jimmy!
return shooting