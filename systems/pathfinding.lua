--> Functions that renders entities through a single sprite!
local pathfinding = hooECS.System.create("pathfinding")

function pathfinding:initialize()
	hooECS.System.initialize(self)
end

--> Find closest totem!
function pathfinding:findClosestTotem(entity)

	local eList = engine:getEntitiesWithComponent("isTotem")
	local closest = eList[1]
	local closestDist = math.huge
	if #eList > 1 then
		for i,v in pairs(eList) do
			local ep = entity:get("position"):fetch()
			local ap = v:get("position"):fetch()
			local dist = math.dist(ep.x, ep.y, ap.x, ap.y)
			if dist < closestDist then
				closestDist = dist
				closest = v
			end
		end
	end
	return closest

end

--> Add path to entity!
function pathfinding:addPathToEntity(entity)

	local map = solver.tileMap
	local closestTotem = self:findClosestTotem(entity)

	local start = map:tileCoordsToTileIndices(entity:get("position"):fetch())
	local goal = map:tileCoordsToTileIndices(closestTotem:get("position"):fetch())

	entity:get("path").path = aStar:aStar(start, goal)

end

--> Advance to next target tile from path!
function pathfinding:setNextGoal(e)
	local nxt = table.remove(e:get("path").path, 1)
	if nxt then
		local mt = solver.tileMap:tileIndicesToWorldCoords(nxt)
		e:get("path").moveTowards = {x = mt.x-1, y = mt.y-1} --> Stupid-ass workaround for tiles!
	end
end

--> Advance to next tile in path!
function pathfinding:moveTowards(entity, dt)
	local gPos = entity:get("path").moveTowards
	if gPos then
		local pos = entity:get("position")
		local dir = math.angle(pos.x, pos.y, gPos.x, gPos.y)
		entity:get("direction").direction = dir
		pos.x = pos.x + entity:get("speed").speed * dt * math.cos(dir)
		pos.y = pos.y + entity:get("speed").speed * dt * math.sin(dir)

		--> If there, remove current target!
		if math.dist(pos.x, pos.y, gPos.x, gPos.y) < entity:get("speed").speed*dt then
			entity:get("path").moveTowards = false
		end
		
	end
	
end


function pathfinding:update(dt)
	for key, entity in pairs(self.targets) do

		--> If there is no path, create one!
		if not entity:get("path").path then
			self:addPathToEntity(entity)
		end

		--> If there is no current goal tile, set one!
		if not entity:get("path").moveTowards then
			self:setNextGoal(entity)
		end

		--> Now move towards that current goal tile!
		self:moveTowards(entity, dt)

	end
end

function pathfinding:requires()
	return {"path", "position", "speed", "direction"}
end

return pathfinding
