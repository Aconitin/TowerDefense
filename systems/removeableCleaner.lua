--> This system removed old entities!
local removeableCleaner = hooECS.System.create("removeableCleaner")

function removeableCleaner:initialize()
	hooECS.System.initialize(self)
end

--> Main update call!
function removeableCleaner:update(dt)
	for _,e in pairs(self.targets) do
		if e:get("removeable").removeable then
			e:deactivate()
			local eng = e:getEngine()
			eng:removeEntity(e)
		end
	end
end

--> Set targets for this system!
function removeableCleaner:requires()
	return({"removeable"})
end

return removeableCleaner