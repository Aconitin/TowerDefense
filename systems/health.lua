--> Functions that renders entities through a single sprite!
local health = hooECS.System.create("health")

function health:initialize()
	hooECS.System.initialize(self)
end

function health:draw()

	for key, entity in pairs(self.targets) do
				
		--> Get components!
		local hc = entity:get("health")
		local pc = entity:get("position")
		
		--> Initialize some constants!
		local barLength, barHeight = 80, 15
		local barOffsetY = entity:get("health").barOffsetY or 25
		local deductible = 7.5
		local x = pc.x * globals.unitScale.x
		local y = pc.y * globals.unitScale.y

		--> Draw the black background for the bar!
		love.graphics.setColor(0, 0, 0, 255)
		love.graphics.rectangle("fill", x - (0.5 * barLength), y - barOffsetY, barLength, barHeight)

		--> Some more variables for the smaller (actual) health bar!
		barLength, barHeight = barLength - deductible, barHeight - deductible
		barOffsetY = barOffsetY - 0.5 * barHeight

		--> Draw the white hp bar indicating damage!
		love.graphics.setColor(0, 255, 255, 255)
		love.graphics.rectangle("fill", x - 0.5 * barLength, y - barOffsetY, barLength * hc.whiteHPBarLength, barHeight)

		--> Finalize the hp bar color!
		local r = math.clamp(0, ((1 - hc.health) * 255 * 2) - 25, 255)
		local g = math.clamp(0, (math.clamp(0, 2 * hc.health, 1) * 255) - 25, 255)

		--> Draw real hp bar!
		love.graphics.setColor(r, g, 0, 255)
		love.graphics.rectangle("fill", x - 0.5 * barLength, y - barOffsetY, barLength * hc.health, barHeight)

	end
end

function health:update(dt)

	for key, entity in pairs(self.targets) do

		local hc = entity:get("health")
	
		--> If this entity received damage, proceed!
		if hc.pdmg ~= 0 then

			hc.gotHit = true
			hc.health = math.clamp(0, hc.health - hc.pdmg, math.huge)
			hc.pdmg = 0

			--> Tween white hp bar!
			if hc.whiteHPBarLengthTweenHandle then
				timer.cancel(hc.whiteHPBarLengthTweenHandle)
				hc.whiteHPBarLengthTweenHandle = nil
			end
			hc.whiteHPBarLengthTweenHandle = timer.tween(0.6, hc, {whiteHPBarLength = hc.health}, "quint")
		end
		
		if hc:isDead() then
			entity:get("removeable"):remove()
		end

	end

end

function health:requires()
	return {"position", "health", "removeable"}
end

return health