--> Functions that renders entities through a single sprite!
local debugRenderEntityPaths = hooECS.System.create("debugRenderEntityPaths")

function debugRenderEntityPaths:initialize()
	hooECS.System.initialize(self)
end

function debugRenderEntityPaths:draw()
	if debugging.renderPath then
		for key, entity in pairs(self.targets) do
		
			local newPath = {}
			local map = solver.tileMap
			for _,p in ipairs(entity:get("path").path) do
				local test = map:tileIndicesToGlobalCoords({i = p.i-1, j = p.j-1})
				table.insert(newPath, test.x)
				table.insert(newPath, test.y)
			end
			if #newPath >= 4 then
				love.graphics.line(newPath)
			end
			
		end
	end
end

function debugRenderEntityPaths:requires()
	return {"path"}
end

return debugRenderEntityPaths