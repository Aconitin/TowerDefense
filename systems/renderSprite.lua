--> Functions that renders entities through a single sprite!
local renderSprite = hooECS.System.create("renderSprite")

function renderSprite:initialize()
	hooECS.System.initialize(self)
end

function renderSprite:draw()
	for key, entity in pairs(self.targets) do
	
		local color =  entity:get("color").color
		love.graphics.setColor(color)
		
		local x = entity:get("position").x * _G.globals.unitScale.x
		local y = entity:get("position").y * _G.globals.unitScale.y
		local orientation =  entity:get("orientation").orientation
		local sprite = entity:get("sprite").sprite
		local sx = entity:get("scale").sx
		local sy = entity:get("scale").sy
		
		--> If entity has a size component, scale it to that size!
		if entity:has("size") then
			local size = entity:get("size")
			
			--> Get size in pixels!
			local width = size.width * _G.globals.unitScale.x
			local height = size.height * _G.globals.unitScale.y
			
			--> Get real scale!
			sx = sx * (width / sprite:getWidth())
			sy = sy * (height / sprite:getHeight())

		end
		
		love.graphics.draw(sprite, x, y, orientation, sx, sy, 0.5 * sprite:getWidth(), 0.5 * sprite:getHeight())
		
	end
end

function renderSprite:requires()
	return {"color", "orientation", "sprite", "scale", "position"}
end

return renderSprite