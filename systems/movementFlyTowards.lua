--> Flies an entity towards given coordinates!
local flyTowards = hooECS.System.create("flyTowards")

function flyTowards:initialize()
	hooECS.System.initialize(self)
end

function flyTowards:update(dt)
	for key, entity in pairs(self.targets) do
		
		local ft = entity:get("flyTowards")
		local pos = entity:get("position")
		local s = entity:get("speed").speed
		
		--> If the flyTowards component is bound to an entity, first update the flyTowards position!
		if ft.boundToTarget then
			local tpos = entity:get("target").target:get("position"):fetch()
			ft.x, ft.y = tpos.x, tpos.y
		end
		
		--> Set arrived flag!
		if ft:hasArrived(pos.x, pos.y, s*dt) then
			ft.arrived = true
			if ft.onArrive then
				ft.onArrive(entity)
			end
		end
		
		--> Now move the entity into the direction of the target position!
		local angle = math.angle(pos.x, pos.y, ft.x, ft.y)
		pos.x = pos.x + dt * s * math.cos(angle)
		pos.y = pos.y + dt * s * math.sin(angle)
		
		--> Now autoOrient the entity if this is activated!
		if ft.autoOrient and not ft.arrived then
			entity:get("orientation"):set(angle)
		end

	end
end

function flyTowards:requires()
	--[[OPTIONAL: 'target', 'orientation' ]]--
	return {"flyTowards", "position", "speed"}
end

return flyTowards
