--> Functions that renders entities through a single sprite!
local debugCursor = hooECS.System.create("debugCursor")

function debugCursor:initialize()
	hooECS.System.initialize(self)
end

function debugCursor:draw()
	if debugging.renderCursorField then
		for key, entity in pairs(self.targets) do
		
			local x = love.mouse.getX()
			local y = love.mouse.getY()
			
			x, y = camera.camera:worldCoords(x, y)
			local i, j = tileMap:convert("pcti", x, y)
			x, y = tileMap:convert("tipc", i, j)

			love.graphics.setColor(0, 255, 255, 100)
			love.graphics.rectangle("fill", x, y, globals.unitScale.x, globals.unitScale.y)
		end
	end
end

function debugCursor:requires()
	return {"isCursor"}
end

return debugCursor