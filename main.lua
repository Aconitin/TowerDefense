-- 88
-- 88                                                              ,d
-- 88                                                              88
-- 88  88,dPYba,,adPYba,   8b,dPPYba,    ,adPPYba,   8b,dPPYba,  MM88MMM  ,adPPYba,
-- 88  88P'   "88"    "8a  88P'    "8a  a8"     "8a  88P'   "Y8    88     I8[    ""
-- 88  88      88      88  88       d8  8b       d8  88            88      `"Y8ba,
-- 88  88      88      88  88b,   ,a8"  "8a,   ,a8"  88            88,    aa    ]8I
-- 88  88      88      88  88`YbbdP"'    `"YbbdP"'   88            "Y888  `"YbbdP"'
--                         88
--                         88

--> Import Stuff!
require("lib.extendedMath") --> Some more math helper functions. Extends the math module, hence not bound to global variable!
random = require("lib.random") --> Random functions!
timer = require("lib.timer") --> Timer library!
inspect = require("lib.inspect") --> Nice debug printing for tables!
cargo = require("lib.cargo") --> Resource manager!
hooECS = require("lib.hooECS") --> ECS!
geometry2d = require("lib.geometry2d.geometry2d"):new()
utils = require("lib.utils")
resources = cargo.init("resources") --> Initialize resource folder!

debugging = require("debugging") --> Messy class for debugging stuff!
view = require("view") --> Camera wrapper class for ingame viewports!
shaders = require("shaders") --> For nice effects!
aStar = require("aStar") --> Pathfinding!
roundKeeper = require("waves.roundKeeper") --> Wave generator and deployment system!

gameStates = require("gameStates.gameStates")

tileMap = require("tiles.tileMap")
tileData = require("tiles.tileData")
mapping = require("tiles.mapping")
tmBasic = require("tiles.tmBasic")
mapDrawingFunctions = require("tiles.mapDrawingFunctions")
mapSolver = require("tiles.mapSolver")

tower = require("tower")


-- 88888888ba                                       88
-- 88      "8b                                      88
-- 88      ,8P                                      88
-- 88aaaaaa8P'  ,adPPYYba,  8b,dPPYba,   ,adPPYba,  88,dPPYba,    ,adPPYba,   8b,dPPYba,    ,adPPYba,  ,adPPYba,
-- 88""""""8b,  ""     `Y8  88P'   "Y8  a8P_____88  88P'    "8a  a8"     "8a  88P'   `"8a  a8P_____88  I8[    ""
-- 88      `8b  ,adPPPPP88  88          8PP"""""""  88       d8  8b       d8  88       88  8PP"""""""   `"Y8ba,
-- 88      a8P  88,    ,88  88          "8b,   ,aa  88b,   ,a8"  "8a,   ,a8"  88       88  "8b,   ,aa  aa    ]8I
-- 88888888P"   `"8bbdP"Y8  88           `"Ybbd8"'  8Y"Ybbd8"'    `"YbbdP"'   88       88   `"Ybbd8"'  `"YbbdP"'

--> Gets called once on game start!
function love.load(args)

	------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------

	--> Get hooECS up and running!
	hooECS.initialize({debug = true})

	--> Register all components with Hoo!
	require("components.target") --> Reference to arbitrary entity!
	require("components.flyTowards") --> Flies an entity towards given x & y coordinate. Can be bound to a 'target' entity that updates said coordinate!
	require("components.position") --> A simple x/y position!
	require("components.speed") --> Speed/velocity in 'meters'/'tiles' per second!
	require("components.removeable") --> A boolean flag that as soon as it becomes true, causes a system to remove this entity from the engine permanently!
	
	require("components.color")
	require("components.direction")
	require("components.orientation")
	require("components.scale")
	require("components.velocity")
	require("components.sprite")
	require("components.health")
	require("components.path")
	require("components.isTotem")
	require("components.size")
	require("components.range")
	require("components.countdown")
	require("components.isCursor")
	require("components.isEnemy")
	require("components.isTower")
	require("components.isBullet")

	--> Register list of entities!
	entities = require("entities")

	--> hooECS engine!
	engine = hooECS.Engine()

	--> Register systems with engine!
	local healthSystem = require("systems.health")()
	engine:addSystem(healthSystem, "update")
	engine:addSystem(healthSystem, "draw")
	
	engine:addSystem(require("systems.renderSprite")(), "draw")
	engine:addSystem(require("systems.pathfinding")(), "update")
	engine:addSystem(require("systems.movementFlyTowards")(), "update")
	engine:addSystem(require("systems.debugRenderEntityPaths")(), "draw")
	engine:addSystem(require("systems.debugCursor")(), "draw")
	
	local shootingSystem = require("systems.shooting")()
	engine:addSystem(shootingSystem, "update")
	engine:addSystem(shootingSystem, "draw") --> Just for debugging!
		
	engine:addSystem(require("systems.removeableCleaner")(), "update")

	------------------------------------------------------------------------------------------------------------
	------------------------------------------------------------------------------------------------------------

	--> Set gameStates!
	gameStates:setActive("mainGame")

end

--> Draw game loop!
function love.draw()
	if gameStates.active.draw then
		gameStates.active:draw()
	end
end

--> Update game loop!
function love.update(dt)
	moved(dt)
	if gameStates.active.update then
		gameStates.active:update(dt)
	end
end


  -- ,ad8888ba,               88  88  88                                   88
 -- d8"'    `"8b              88  88  88                                   88
-- d8'                        88  88  88                                   88
-- 88             ,adPPYYba,  88  88  88,dPPYba,   ,adPPYYba,   ,adPPYba,  88   ,d8   ,adPPYba,
-- 88             ""     `Y8  88  88  88P'    "8a  ""     `Y8  a8"     ""  88 ,a8"    I8[    ""
-- Y8,            ,adPPPPP88  88  88  88       d8  ,adPPPPP88  8b          8888[       `"Y8ba,
 -- Y8a.    .a8P  88,    ,88  88  88  88b,   ,a8"  88,    ,88  "8a,   ,aa  88`"Yba,   aa    ]8I
  -- `"Y8888Y"'   `"8bbdP"Y8  88  88  8Y"Ybbd8"'   `"8bbdP"Y8   `"Ybbd8"'  88   `Y8a  `"YbbdP"'

function love.keypressed(key, scancode, isRepeat)
	if gameStates.active.keyPressed then
		gameStates.active:keyPressed(key, scancode, isRepeat)
	end
end

function love.keyreleased(key, scancode)
	if gameStates.active.keyReleased then
		gameStates.active:keyReleased(key, scancode)
	end
end

function love.mousemoved(x, y, dx, dy, istouch)
	if gameStates.active.mouseMoved then
		gameStates.active:mouseMoved(x, y, dx, dy, istouch)
	end
end

function love.mousepressed(x, y, button, istouch)
	if gameStates.active.mousePressed then
		gameStates.active:mousePressed(x, y, button, istouch)
	end
end

function love.mousereleased(x, y, button, istouch)
	if gameStates.active.mouseReleased then
		gameStates.active:mouseReleased(x, y, button, istouch)
	end
end

function love.resize(w, h)
	if gameStates.active.resize then
		gameStates.active:resize(w, h)
	end
end

function love.textinput(text)
	if gameStates.active.textInput then
		gameStates.active:textInput(text)
	end
end

function love.wheelmoved(dx, dy)
	if gameStates.active.wheelMoved then
		gameStates.active:wheelMoved(dx, dy)
	end
end

function moved(dt)

	--> Update movement!
	local dx, dy = 0, 0
	if love.keyboard.isDown("a", "left", "kp4") then --> Event: Move (camera) to the left!
		dx = dx - dt
	end
	if love.keyboard.isDown("d", "right", "kp6") then --> Event: Move (camera) to the right!
		dx = dx + dt
	end
	if love.keyboard.isDown("w", "up", "kp8") then --> Event: Move (camera) up!
		dy = dy - dt
	end
	if love.keyboard.isDown("s", "down", "kp2") then --> Event: Move (camera) down!
		dy = dy + dt
	end

	if gameStates.active.moved then
		gameStates.active:moved(dx, dy)
	end

end


-- 88888888888                          88                                             88
-- 88                                   88                                             88
-- 88                                   88                                             88
-- 88aaaaa      8b,dPPYba,  8b,dPPYba,  88,dPPYba,   ,adPPYYba,  8b,dPPYba,    ,adPPYb,88
-- 88"""""      88P'   "Y8  88P'   "Y8  88P'    "8a  ""     `Y8  88P'   `"8a  a8"    `Y88
-- 88           88          88          88       88  ,adPPPPP88  88       88  8b       88
-- 88           88          88          88       88  88,    ,88  88       88  "8a,   ,d88
-- 88888888888  88          88          88       88  `"8bbdP"Y8  88       88   `"8bbdP"Y8

function love.errhand(msg)
	msg = tostring(msg)

	print((debug.traceback("Error: " .. tostring(msg), 1+(2 or 1)):gsub("\n[^\n]+$", "")))

	if not love.window or not love.graphics or not love.event then
		return
	end

	if not love.graphics.isCreated() or not love.window.isOpen() then
		local success, status = pcall(love.window.setMode, 800, 600)
		if not success or not status then
			return
		end
	end

	if love.mouse then
		love.mouse.setVisible(true)
		love.mouse.setGrabbed(false)
		love.mouse.setRelativeMode(false)
	end
	if love.joystick then
		for i,v in ipairs(love.joystick.getJoysticks()) do
			v:setVibration()
		end
	end
	if love.audio then love.audio.stop() end
	love.graphics.reset()
	local font = love.graphics.setNewFont(math.floor(love.window.toPixels(14)))

	love.graphics.setBackgroundColor(89, 157, 220)
	love.graphics.setColor(255, 255, 255, 255)

	local trace = debug.traceback()

	love.graphics.clear(love.graphics.getBackgroundColor())
	love.graphics.origin()

	local err = {}

	table.insert(err, "Hilariously catastrophic error!\n")
	table.insert(err, "Whoops, that shouldn't have happened.\nPlease share the information below with the author of the game to get this fixed.\nThanks!\n\n\n\n")
	table.insert(err, "--> " .. msg.."\n\nTraceback:")

	local _ = 0
	for l in string.gmatch(trace, "(.-)\n") do
		if not string.match(l, "boot.lua") and not string.match(l, "stack traceback:") then
			table.insert(err, tostring(_) .. " --> " .. l)
			_ = _ + 1
		end
	end

	local p = table.concat(err, "\n")

	p = string.gsub(p, "\t", "")
	p = string.gsub(p, "%[string \"(.-)\"%]", "%1")

	local function draw()
		local pos = love.window.toPixels(70)
		love.graphics.clear(love.graphics.getBackgroundColor())
		love.graphics.printf(p, pos, pos, love.graphics.getWidth() - pos)
		love.graphics.present()
	end

    local exit = false
	while true do
		love.event.pump()

		for e, a, b, c in love.event.poll() do
			if e == "quit" then
				return
			elseif e == "keypressed" and a == "escape" then
				return
            elseif e == "keypressed" and a == "f5" then
                exit = true
			elseif e == "touchpressed" then
				local name = love.window.getTitle()
				if #name == 0 or name == "Untitled" then name = "Game" end
				local buttons = {"OK", "Cancel"}
				local pressed = love.window.showMessageBox("Quit "..name.."?", "", buttons)
				if pressed == 1 then
					return
				end
			end
		end

        if exit then
            break
        end

		draw()

		if love.timer then
			love.timer.sleep(0.1)
		end
	end
    if exit then
        love.event.quit("restart")
    end

end

-- 88888888ba               88
-- 88      "8b              ""                ,d
-- 88      ,8P                                88
-- 88aaaaaa8P'  8b,dPPYba,  88  8b,dPPYba,  MM88MMM
-- 88""""""'    88P'   "Y8  88  88P'   `"8a   88
-- 88           88          88  88       88   88
-- 88           88          88  88       88   88,
-- 88           88          88  88       88   "Y888

--> Override for print to print nice tables!
local oldPrint = print
function print(...)
	if not ... then
		oldPrint("nil")
    elseif type(...) == "table" then
        oldPrint(inspect.inspect(...))
    else
        oldPrint(...)
    end
end

-- 88
-- 88
-- 88
-- 88   ,adPPYba,   8b       d8   ,adPPYba,       8b,dPPYba,  88       88  8b,dPPYba,
-- 88  a8"     "8a  `8b     d8'  a8P_____88       88P'   "Y8  88       88  88P'   `"8a
-- 88  8b       d8   `8b   d8'   8PP"""""""       88          88       88  88       88
-- 88  "8a,   ,a8"    `8b,d8'    "8b,   ,aa  888  88          "8a,   ,a88  88       88
-- 88   `"YbbdP"'       "8"       `"Ybbd8"'  888  88           `"YbbdP'Y8  88       88

--> Custom override for love.run to provide fixed dt!
function love.run()

	if love.math then
		love.math.setRandomSeed(os.time())
	end

	if love.load then love.load(arg) end

	-- We don't want the first frame's dt to include time taken by love.load.
	if love.timer then love.timer.step() end

	local dt = 0

	-- Main loop time.
	while true do
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end

		-- Update dt, as we'll be passing it to update
		if love.timer then
			love.timer.step()
			dt = love.timer.getDelta()
		end

		-- Call update and draw
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled

		if love.graphics and love.graphics.isActive() then
			love.graphics.clear(love.graphics.getBackgroundColor())
			love.graphics.origin()
			if love.draw then love.draw() end
			love.graphics.present()
		end

		if love.timer then love.timer.sleep(0.001) end
	end

end
