local removeable = hooECS.Component.create('removeable')

function removeable:initialize(r)
	self.removeable = r or false
end

function removeable:set(r)
	self.removeable = r
end

function removeable:remove()
	self.removeable = true
end