local size = hooECS.Component.create('size')

--> Size table st is a table containing either width, or height, or both. "ratio" is the width/height ratio of the sprite!
function size:initialize(st, ratio)

	local width = st.width
	local height = st.height
	local ratio = ratio or 1

	if (not width) and height then
		width = ratio * height
	end
	
	if (not height) and width then
		height = width / ratio
	end

	self.width = width
	self.height = height
	self.ratio = self.width / self.height
	
end