local countdown = hooECS.Component.create('countdown')

function countdown:initialize(max)
	self.max = max or 5
	self.current = self.max
	self.ready = false
end

function countdown:count(dt)
	if not self.ready then
		self.current = math.clamp(0, self.current - dt, math.huge)
		if self.current <= 0 then
			self.ready = true
		end
	end
end

function countdown:reset()
	self.current = self.max
	self.ready = false
end

function countdown:getPerc()
	return 1-(self.current/self.max)
end