local orientation = hooECS.Component.create('orientation')

function orientation:initialize(o)

	self.orientation = o or math.rad(0)

end

function orientation:set(o)

	self.orientation = o

end