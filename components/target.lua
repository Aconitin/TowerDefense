--> A target is always a reference to another entity. This can be an aiming target for a tower, or a target used in the moveTowards component that e.g. a bullet uses!
local target = hooECS.Component.create("target")

function target:initialize(t)

	self.target = t or nil
	
end

function target:set(t)

	self.target = t

end