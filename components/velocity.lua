local velocity = hooECS.Component.create('velocity')

function velocity:initialize(vx, vy)

	self.vx = vx or 0
	self.vy = vy or 0

end