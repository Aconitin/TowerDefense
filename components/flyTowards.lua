--> This component specifies a x and y coordinate that the holding entity will be 'flown' towards, i.e. moved towards without respect to walls/objects in the way. Used for e.g. the bullet!
local flyTowards = hooECS.Component.create("flyTowards")

function flyTowards:initialize(x, y, onArrive, boundToTarget, autoOrient)

	self.x = x or 0
	self.y = y or 0
	self.boundToTarget = boundToTarget or false --> If this is true, each frame the x and y coordinate of this component will update to the x/y coordinate of the target component that the holding entity _has to hold_!
	self.arrived = false --> Gets turned to true if holding entity "is there"!
	self.autoOrient = autoOrient or false --> If true, automatically sets the holding entity's orientation to the direction the bullet flies towards!
	self.onArrive = onArrive or nil --> Function that gets called with the holding entity as first parameter as soon as the entity arrived!
	
end

--> Returns true if the speed of the entity is enough to arrive at the target within the next frame. Note that 'speed' has to be entity movement speed times dt!
function flyTowards:hasArrived(x, y, speed)
	if math.dist(x, y, self.x, self.y) <= speed then
		return true
	end
	return false
end