local health = hooECS.Component.create('health')

function health:initialize(vulnerability, startHealth)

	self.vulnerability = vulnerability or 1 --> Constant factor onto damage!
	self.health = startHealth or 1 --> Value between 0 and 1!
	self.pdmg = 0 --> Pending damage taken between last and current update call!
	self.whiteHPBarLength = 1 --> Length in percent [0-1] of tweened hp bar!
	self.gotHit = false --> For flashing on hit!
	self.whiteHPBarLengthTweenHandle = nil --> Same!
	self.barOffsetY = 50 --> Position of health bar relative to center point of enemy!

end

function health:doDamage(dmg)
	self.pdmg = self.pdmg + dmg * self.vulnerability
end

function health:isDead()
	return self.health <= 0
end

function health:isProbablyDead()
	return (self.health - self.pdmg) <= 0
end

function health:getMaxHealth()
	return (1/self.vulnerability)
end

function health:getComparable()
	return self:getMaxHealth() * self.health
end

function health:isGreaterThan(h)
	return self:getComparable() > h:getComparable()
end