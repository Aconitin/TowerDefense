local position = hooECS.Component.create('position')

function position:initialize(x, y)

	self.x = x or 0
	self.y = y or 0
	
end

function position:fetch()

	return({x = self.x, y = self.y})
	
end