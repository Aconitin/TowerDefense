local scale = hooECS.Component.create('scale')

function scale:initialize(sx, sy)

	self.sx = sx or 1
	self.sy = sy or 1
	
end