local color = hooECS.Component.create('color')

function color:initialize(r, g, b, a)

	local r = r or 255
	local g = g or 255
	local b = b or 255
	local a = a or 255

	self.color = {r, g, b, a}
	
end