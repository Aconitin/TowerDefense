import sys
from PIL import Image

def process(inPath, outPath, tileSize):
	inImage = Image.open(inPath)

	width, height = inImage.size
	hTiles = width / tileSize
	vTiles = height / tileSize
	outTileSize = tileSize + 2

	outImage = inImage.resize((hTiles * outTileSize, vTiles * outTileSize))

	print "Input file:\n\tpath: {0}\n\tsize: {1}\nOutput file:\n\tpath: {2}\n\tsize: {3}".format(
		inPath, inImage.size, outPath, outImage.size
	)

	for j in range(vTiles):
		for i in range(hTiles):
			for jj in range(tileSize):
				inY = j * tileSize + jj
				outY = j * outTileSize + jj + 1
				for ii in range(tileSize):
					inX = i * tileSize + ii
					outX = i * outTileSize + ii + 1
					color = inImage.getpixel((inX, inY))
					outImage.putpixel((outX, outY), color)
					if ii == 0:
						outImage.putpixel((outX - 1, outY), color)
						if jj == 0:
							outImage.putpixel((outX - 1, outY - 1), color)
					if jj == 0:
						outImage.putpixel((outX, outY - 1), color)

	outImage.save(outPath)

if __name__ == "__main__":
	numArgs = len(sys.argv)
	if numArgs != 4:
		print "usage: %s <inFile> <outFile> <tileSize>" % sys.argv[0]
		sys.exit(2)
	process(sys.argv[1], sys.argv[2], int(sys.argv[3]))
